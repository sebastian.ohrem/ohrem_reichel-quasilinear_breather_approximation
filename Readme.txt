To obtain images for the paper "Existence of traveling breather solutions to cubic Nonlinear maxwell equations in waveguide geometries":
- open MATLAB in \scripts\ and run main.m
- breathers will be generated, drawn into current figure, and saved
- possibly adjust figure size before running (call e.g. clf to clear/create a figure)
- images and breather coefficients can be found in \saves\
- execution generates temporary image files, which (depending on MATLAB settings) may land in your trash

Images from paper were obtained using:
- Matlab R2023a Update 3
- Optimization Toolbox version 9.5

Many thanks go to Willy Dörfler (KIT) for providing the discontinuous galerkin method code in \dG\.