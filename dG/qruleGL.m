function [q,qw] = qruleGL(deg,fvals,psi1,psi2)
%% Call: [q,qw] = qruleGL(deg,fvals,psi1,psi2);
% Input:
%    deg ... INT; Degree of exactness for polynomials.
%    fvals ... (*,1); Evaluation of the integrand in the quadrature
%          nodes.
%    psi1, psi2 ... (*,deg+1 (deg+2)); Basis function evaluations.
% Output:
%    q ... nargin==1: (*,1); Quadrature nodes.
%          nargin==2: (*,1); Integral(s) in >2-arg call
%    qw ... nargout==2: (*,1); Quadrature weights.
% Description:
% It is necessary to call 'qruleGL' once with no argument to initialize
% the quadrature formulas up to degree 'degmax'.
% Using one argument 'deg', it returns the quadrature nodes that are
% exact for polynomials of degree p='deg' using the Gauss-Legendre
% quadrature rule on [0,1] (!). For 'deg>degmax' the quadrature formula
% is newly created.
% With two arguments, the quadrature rule is applied using the function
% values 'fvals', with three or four it integrates f*Psi, f*Psi1*Psi2
% respectively.
%%

% Developed at IAMN 2, Univ. Karlsruhe. Matlab 6.1.0450.
% Last modified: 05.10.2002, W. Doerfler, Univ. Karlsruhe.
%                06.01.2014, W. Doerfler, KIT.
%                27.11.2020, W. Doerfler, KIT.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Globals
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global degmax qpts qwts s_deg s_qn s_qw;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Distinguish by number of arguments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch nargin
case 1
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % Case: Return quadrature nodes in [0,1] !
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   if isempty(degmax), qruleGL; end% Initialize if not done before
   noqp = ceil((deg+1)/2);% Number of necessary quadrature points
   if deg<=degmax% For small degrees 
      q = qpts(noqp,1:noqp)';% ... use predefined values
   else% Otherwise compute new ones (method from Davis and Rabinowitz,
       % algorithm Wilson's book)
      uu = (1:noqp-1)./realsqrt(4*(1:noqp-1).^2-1);% Compute base points
      [vc,bp] = eig(diag(uu,-1)+diag(uu,1));% ... and weight factors on [-1,1]
      [bp,kk] = sort(diag(bp)); wf = 2*vc(1,kk).^2;
      if mod(noqp,2), bp((noqp+1)/2) = 0; end% Improve !
      s_deg = deg; s_qn = (bp+1)/2; s_qw = wf/2;% Transform to [0,1]
      q = s_qn;
   end
case 2
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % Case: Perform numerical integration: \int f
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   if deg<=degmax% For small degrees 
      noqp = ceil((deg+1)/2);% Number of necessary quadrature points
      q = qwts(noqp,1:noqp)*fvals;
   elseif deg==s_deg% The one on stack only !
      q = s_qw*fvals;
   else
      error('*** ERROR in qruleGL.m *** No such case');
   end
case 3
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % Case: Perform numerical integration: \int f Psi
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % Note: symmetry of qwts not used
   if deg<=degmax% For small degrees 
      noqp = ceil((deg+1)/2);% Number of necessary quadrature points
      q = psi1'*(fvals.*qwts(noqp,1:noqp)');
   elseif deg==s_deg% The one on stack only !
      q = psi1'*(fvals.*s_qw');
   else
      error('*** ERROR in qruleGL.m *** No such case');
   end
case 4
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % Case: Perform numerical integration: \int f Psi1 Psi2
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   if deg<=degmax% For small degrees 
      noqp = ceil((deg+1)/2);% Number of necessary quadrature points
      q = psi1'*diag(fvals.*qwts(noqp,1:noqp)')*psi2;
   elseif deg==s_deg% The one on stack only !
      q = psi1'*diag(fvals.*s_qw')*psi2;
   else
      error('*** ERROR in qruleGL.m *** No such case');
   end
case 0
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % Case: Initialise quadrature nodes in [0,1] !
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   degmax = 13; qptsmax= (degmax+1)/2;
   qpts = zeros(qptsmax,qptsmax);
   qwts = zeros(qptsmax,qptsmax);
   % The given formulas are quadrature rules in [-1,1] ! They will be
   % transformed to [0,1] afterwards.
   % (*) Calculated from netlib-software with FORTRAN real*16
   % Symbolic Formulas up to deg=5: http://de.wikipedia.org/wiki/Gau%C3%9F-Quadratur
   % Explicit tables up to deg=64: http://pomax.github.io/bezierinfo/legendre-gauss.html
   qpts(1,1:1)= 0;
   qwts(1,1:1)= 2;
   qpts(2,1:2)= [-1,1]*0.57735026918962576451;% (*)
   qwts(2,1:2)= [ 1,1];
   qpts(3,1:3)= [-1,0,1]*0.77459666924148337704;% (*)
   qwts(3,1:3)= [ 0.55555555555555555556, ...
                  0.88888888888888888889, ...
                  0.55555555555555555556];
   qpts(4,1:4)= [-0.86113631159405257522, ...
                 -0.33998104358485626480, ...
                  0.33998104358485626480, ...
                  0.86113631159405257522];% (*)
   qwts(4,1:4)= [ 0.34785484513745385737, ...
                  0.65214515486254614263, ...
                  0.65214515486254614263, ...
                  0.34785484513745385737];% (*)
   qpts(5,1:5)= [-0.90617984593866399280, ...
                 -0.53846931010568309104, ...
                  0, ...
                  0.53846931010568309104, ...
                  0.90617984593866399280];% (*)
   qwts(5,1:5)= [ 0.23692688505618908751, ...
                  0.47862867049936646804, ...
                  0.56888888888888888889, ...
                  0.47862867049936646804, ...
                  0.23692688505618908751];% (*)
   qpts(6,1:6)= [-0.93246951420315202781, ...
                 -0.66120938646626451366, ...
                 -0.23861918608319690863, ...
                  0.23861918608319690863, ...
                  0.66120938646626451366, ...
                  0.93246951420315202781];% (*)
   qwts(6,1:6)= [ 0.17132449237917034504, ...
                  0.36076157304813860757, ...
                  0.46791393457269104739, ...
                  0.46791393457269104739, ...
                  0.36076157304813860757, ...
                  0.17132449237917034504];% (*)
   qpts(7,1:7)= [-0.94910791234275852453, ...
                 -0.74153118559939443986, ...
                 -0.40584515137739716691, ...
                  0, ...
                  0.40584515137739716691, ...
                  0.74153118559939443986, ...
                  0.94910791234275852453];% (*)
   qwts(7,1:7)= [ 0.12948496616886969327, ...
                  0.27970539148927666790, ...
                  0.38183005050511894495, ...
                  0.41795918367346938776, ...
                  0.38183005050511894495, ...
                  0.27970539148927666790, ...
                  0.12948496616886969327];% (*)
   % Transformation to [0,1]
   qpts = (qpts+1)/2; for k=1:qptsmax, qpts(k,k+1:qptsmax) = 0; end
   qwts = qwts/2;
otherwise
   error('*** ERROR in qruleGL.m *** Check function definition');
end
if nargout==2
   qw = qwts(noqp,1:noqp)';
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
return

