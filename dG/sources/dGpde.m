function pde = dGpde(fun_a,fun_b,fun_c,fun_f,fun_g)
%% CALL: pde = dGpde(fun_a,fun_b,fun_c,fun_f,fun_g);
% INPUT:
%    fun_a, etc ... FUNC/DBLE*; PDE coefficients (see below, [DEF 1 [] ... ]).
% OUTPUT:
%    pde ... STRUCT; PDE coefficients.
% DESCRIPTION:
% DGPDE packs PDE coefficient description into a structure. Unused
% attributes are [] or can be set to [] and are treated as constant 0.
% Note that pde.fstat.a, ... will provide information about the functions'
% status: 0: constant 0, 1: constant, 2: variable in space.
% The general linear second order pde looks like
%    - div (a grad u) + b u + c u = f - div g    in Omega
% providing them with generic names.
%%

% Developed at IANM 2, KIT. Matlab R2011a.
% Version 1.0: Willy Doerfler, Jan 28, 2016.
%%

%% Analyse input
if nargin<1
   fstat = struct('a',1,'b',0,'c',0,'f',0,'g',0);% Default
   pde = struct('fun_a',1,'fun_b',[],'fun_c',[],'fun_f',[],'fun_g',[], ...
                'fstat',fstat);
   return
elseif nargin<2
   fun_b = [];
   fun_c = [];
   fun_f = [];
   fun_g = [];
elseif nargin<3
   fun_c = [];
   fun_f = [];
   fun_g = [];
elseif nargin<4
   fun_f = [];
   fun_g = [];
elseif nargin<5
   fun_g = [];
end

%% Interprete coefficient information
fstat = struct('a',1,'b',1,'c',1,'f',1,'g',1);
if isempty(fun_a), fun_a = 0; fstat.a = 0; end
if isempty(fun_b), fun_b = 0; fstat.b = 0; end
if isempty(fun_c), fun_c = 0; fstat.c = 0; end
if isempty(fun_f), fun_f = 0; fstat.f = 0; end
if isempty(fun_g), fun_g = 0; fstat.g = 0; end
if isa(fun_a,'function_handle'), fstat.a = 2; end
if isa(fun_b,'function_handle'), fstat.b = 2; end
if isa(fun_c,'function_handle'), fstat.c = 2; end
if isa(fun_f,'function_handle'), fstat.f = 2; end
if isa(fun_g,'function_handle'), fstat.g = 2; end

%% Set standard names if not already done
pde = struct('fun_a',fun_a,'fun_b',fun_b,'fun_c',fun_c,'fun_f',fun_f, ...
             'fstat',fstat);

return
