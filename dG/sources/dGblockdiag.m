function A = dGblockdiag(B,k,d)
%% CALL: A = dGblockdiag(B,k,d);
% Returns a blockdiagonal matrix with k times B on the diagonal,
% d is a block-scaling.

% W. Doerfler, KIT, 26.12.2018, dG 2.0.
%%

% Prepare
[m,n] = size(B);   
pq = repmat(1:m,n,1); pp = pq';
ip = pp(:);
iq = pq(:);
ki = zeros(k*m*n,1);
ss = zeros(k*m*n,1);
icm = 0; icn = 0; id = 0;

% Store
if nargin==2% Assume d=ones
   for j=1:k
      ki(id+1:id+m*n) = icm+ip;
      li(id+1:id+m*n) = icn+iq;
      ss(id+1:id+m*n) = B(:);
      icm = icm+m; icn = icn+n;
      id  = id+m*m;
   end
else
   for j=1:k
      ki(id+1:id+m*n) = icm+ip;
      li(id+1:id+m*n) = icn+iq;
      ss(id+1:id+m*n) = d(j)*B(:);
      icm = icm+m; icn = icn+n;
      id  = id+m*m;
   end
end
A = sparse(ki,li,ss,k*n,k*m);

end
