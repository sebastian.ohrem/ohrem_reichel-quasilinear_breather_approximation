function Vp1D = GradVandermonde1D(pd,xi)
%% CALL: Vp1D = GradVandermonde1D(pd,xi);
% Purpose: Initialises the 1D Vandermonde Matrix, V_{ij} = JacobiP'_j(xi_i).
% Hesthaven/Warburton, Nodal Discontinuous Galerkin Methods, 2008.
% Changes by W. Doerfler, KIT.
%%

% Get Vp1D for all required polynomial degrees
Vp1D = GradJacobiP(xi(:),0,0,0:pd);

return
