function xi = JacobiGL(alpha,beta,p)
%% CALL: xi = JacobiGL(alpha,beta,p);
% Purpose: Compute the p-th order Gauss-Lobatto quadrature points xi,
%          associated with the Jacobi polynomial of type
%          (alpha,beta)>-1 ( <> -0.5 ). 
% Hesthaven/Warburton, Nodal Discontinuous Galerkin Methods, 2008.
% Changes by W. Doerfler, KIT.
%%

% Initialisation
xi = zeros(p+1,1);

% Lowest order
if p==1
   xi(1) = -1; xi(2) = 1;
   return
end

% Higher order
xinterior = JacobiGQ(alpha+1,beta+1,p-2);
xi = [-1, xinterior', 1]';

return
