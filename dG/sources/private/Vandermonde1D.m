function V1D = Vandermonde1D(pd,xi)
%% CALL: V1D = Vandermonde1D(pd,xi);
% Purpose: Initialise the 1D Vandermonde matrix, V_{ij} = JacobiP_j(xi_i).
% Hesthaven/Warburton, Nodal Discontinuous Galerkin Methods, 2008.
% Changes by W. Doerfler, KIT.
%%

% Get V1D for all required polynomial degrees
V1D = JacobiP(xi(:),0,0,0:pd);

return
