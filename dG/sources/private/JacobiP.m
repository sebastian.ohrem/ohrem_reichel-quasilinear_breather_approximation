function P = JacobiP(x,alpha,beta,pd)
%% CALL: P = JacobiP(x,alpha,beta,pd);
% Purpose: Evaluates Jacobi polynomial of type (alpha,beta)>-1 (alpha+beta<>-1)
%          at points x for orders in pd and returns P(x,pd).
% Note:    The polynomials are normalised to be orthonormal in L^2(-1,1).
% Hesthaven/Warburton, Nodal Discontinuous Galerkin Methods, 2008.
% Changes by W. Doerfler, KIT.
%%

% Our version allows to get polynomials of a list of degrees. However, we make
% it compatible to the original version.
N = max(pd);

% Turn points into row if needed.
dims = size(x);
if dims(2)==1, x = x'; end

% Initialisation
PL = zeros(N+1,length(x));
%alpha = double(alpha);
%beta  = double(beta);
albe = alpha+beta;

% Initial values P_0(x) and P_1(x).
gamma0  = 2^(albe+1)/(albe+1)*gamma(alpha+1)*gamma(beta+1)/gamma(albe+1);
PL(1,:) = 1.0/realsqrt(gamma0);
if N==0
   P = PL(pd+1,:)';
   return
end
gamma1  = (alpha+1)*(beta+1)/(albe+3)*gamma0;
PL(2,:) = ((albe+2)*x/2 + (alpha-beta)/2)/realsqrt(gamma1);
if N==1
   P = PL(pd+1,:)';
   return
end

% Iterated value in recurrence.
a_old = 2/(albe+2)*realsqrt((alpha+1)*(beta+1)/(albe+3));

% Forward recurrence using the symmetry of the recurrence.
for i=1:N-1
  h = 2*i+albe;
  a_new = 2/(h+2)*realsqrt( (i+1)*(i+1+albe)*(i+1+alpha) ...
          *(i+1+beta)/(h+1)/(h+3) );
  bnew = -(alpha^2-beta^2)/h/(h+2);
  PL(i+2,:) = 1/a_new*( -a_old*PL(i,:) + (x-bnew).*PL(i+1,:) );
  a_old = a_new;
end
P = PL(pd+1,:)';

return
