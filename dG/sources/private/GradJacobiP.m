function dP = GradJacobiP(x,alpha,beta,pd)
%% CALL: dP = GradJacobiP(x,alpha,beta,pd);
% Purpose: Evaluates the derivative of the Jacobi polynomial of type
%          (alpha,beta)>-1 at points x for orders in pd and returns P'(x,pd).        
% Note:    The Jacobi polynomials are normalized to be orthonormal in L^2(-1,1).
% Hesthaven/Warburton, Nodal Discontinuous Galerkin Methods, 2008.
% Changes by W. Doerfler, KIT.
%%

% Our version allows to get polynomials of a list of degrees. However, we make
% it compatible to the original version.
N = max(pd);

% Initialisation
albe = alpha+beta;

% Use relation to JacobiP. Case m=0 stays 0.
m = 1:N;
Dm = diag([0,sqrt(m.*(m+albe+1))]);
dP = JacobiP(x(:),alpha+1,beta+1,[0,0:N-1])*Dm;
dP = dP(:,pd+1);% Restrict to the required set of degrees

return
