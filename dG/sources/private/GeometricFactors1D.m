function [xi_x,J] = GeometricFactors1D(x,Dxi)
%% CALL: [xi_x,J] = GeometricFactors1D(x,Dxi);
% Purpose: Computes the metric elements for the local mappings of the 1D
%          elements.
% Hesthaven/Warburton, Nodal Discontinuous Galerkin Methods, 2008.
% Changes by W. Doerfler, KIT.
%%

x_xi = Dxi*x; J = x_xi; xi_x = 1./J;

return
