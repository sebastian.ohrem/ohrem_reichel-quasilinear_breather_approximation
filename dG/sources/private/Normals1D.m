function nx = Normals1D(Nnpf,Nfpc,Ncell)
%% CALL: nx = Normals1D(Nnpf,Nfpc,Ncell);
% Purpose: Compute outward pointing normals at cell faces.
% Hesthaven/Warburton, Nodal Discontinuous Galerkin Methods, 2008.
% Changes by W. Doerfler, KIT.
%%

% Initialisation
nx = zeros(Nnpf*Nfpc,Ncell); 

% Define outward normals
nx(1,:) = -1.0; nx(2,:) = 1.0;

return
