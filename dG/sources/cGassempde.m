function [L,F,M] = cGassempde(msh,spc,pde)
%% CALL: [L,F,M] = cGassempde(msh,spc,pde);
% INPUT:
%    msh ... STRUCT; (DG)Mesh-structure.
%    spc ... STRUCT; (CG)Space-structure.
%    pde ... STRUCT; PDE coefficients.
% OUTPUT:
%    L ... DOUBLE; Global continuous Galerkin stiffness matrix.
%    F ... DOUBLE; Global continuous Galerkin right hand side vector.
% DESCRIPTION:
% CGASSEMPDE establishes the matrices of the continuous Finite Element method
% for chosen mesh and space and pde-coefficients.
% [L,F] = CGASSEMPDE(MSH,SPC,PDE) integrates
%    L = [ \int a d\Psi_l d\Psi_k
%         +\int b d\Psi_l  \Psi_k
%         +\int c \Psi_l \Psi_k ]_kl,
%    F = [ \int f \Psi_k - g d\Psi_k ]_k,
%    M = [ \int   \Psi_l \Psi_k ]_kl,
% for all global basis elements {\Psi_l}_l.

% Literature: Hesthaven/Warburton 2008.
% W. Doerfler, KIT, 19.08.2017, dG 2.0.
%%

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Switch according to nargout
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargout==1
   [L,B,C] = cGassemble(msh,spc,pde);
elseif nargout==2
   [L,B,C,F] = cGassemble(msh,spc,pde);
elseif nargout==3
   [L,B,C,F,M] = cGassemble(msh,spc,pde);
else
   error('*** Check argument list')
end
L = L+B+C;

%%
return