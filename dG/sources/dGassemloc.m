function [M,Dr,Ds,Lift] = dGassemloc(msh,spc)
%% CALL: [M,Dr,Ds,Lift] = dGassemloc(msh,spc);
% INPUT:
%    msh ... STRUCT; (DG)Mesh-structure.
%    spc ... STRUCT; (DG)Space-structure.
% OUTPUT:
%    M ... DOUBLE; Cell mass matrix as below
%    Dr, Ds ... DOUBLE; Cell matrices as below
%    Lift ... DOUBLE; Extension matrix as below
% DESCRIPTION:
% DGASSEMLOC computes the local mass and differentiation matrices as well
% as the local extension matrix for the given polynomial degree spc.pd on
% the reference cell.
% [M,DR,DS,LIFT] = DGASSEMLOC(MSH,SPC) provides matrices/vectors
%    M  = [ \int    \Psi_l \Psi_k ]_kl,
%    DR = [ \int \dr\Psi_l \Psi_k ]_kl,
%    DS = [ \int \ds\Psi_l \Psi_k ]_kl, msh.DIM>1,
%    LIFT extends boundary basis functions onto reference cell,
% for all reference cell basis elements {\Psi_l}_l, the number depending on
% the polynomial degree spc.pd.
%%

% Literature: Hesthaven/Warburton 2008.
% Version 1.0: Willy Doerfler, KIT, 2015.
%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Switch according to msh.DIM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch msh.DIM

%% 1D %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
case 1

% Mass matrix M_ref
M = (spc.V0*spc.V0')\eye(size(spc.V0));% M = inv(V0*V0')

% Differentiation matrix D_ref
Dr = spc.V1/spc.V0;
Ds = [];% 2D only

% Lift matrix, Lift = inv(M_ref)*Els
Els = zeros(spc.pd+1,spc.Nnpf*msh.Nfpc);
Els(1,1) = 1.0; Els(end,2) = 1.0;
Lift = spc.V0*(spc.V0'*Els);

%% 2D %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
case 2

% Mass matrix M on the reference cell
M = (spc.V0*spc.V0')\eye(size(spc.V0));% M = inv(V0*V0')

% Differentiation matrices D_r, D_s on the reference cell
Dr = spc.Vr/spc.V0;
Ds = spc.Vs/spc.V0;

% Lift matrix, Lift = inv(M)*Els
%Els = zeros(spc.pd+1,spc.Nnpf*msh.Nfpc);
%Els(1,1) = 1.0; Els(end,2) = 1.0;
%Lift = spc.V0*(spc.V0'*Els);
Lift = [];

%% Error %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
otherwise
   error('*** Not implemented')
end

%%
return