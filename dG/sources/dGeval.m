function [Delta,unval,xg,yg] = dGeval(msh,spc,un,qq)
%% CALL: [Delta,unval,xg,yg] = dGeval(msh,spc,un,qq);
% INPUT:
%    msh ... STRUCT; Mesh structure.
%    spc ... STRUCT; Space structure.
%    un ... spDBLE(*,*); Discrete dG-vector in (nodal long or) block form.
%    qq ... DBLE; Local point set in barycentric coordinates ([DEF: barycentres]).
% OUTPUT:
%    Delta ... spDBLE(*,*); Evaluation matrix UNVAL = DELTA*UN (blockform).
%    unval ... DBLE; Evaluation UN(XG) as a dG-vector in block form.
%    xg,yg ... DBLE; Corresponding global point set (depending on MSH.DIM).
% DESCRIPTION:
% DGEVAL evaluates the dG-vector UN on a set of global nodes defined by the
%    barycentric points QQ in the reference element.
% DELTA = DGEVAL(MSH,SPC) returns the evaluation matrix to perform
%    UNVAL = DELTA*UN (blockform) at the barycentres. Same does
% DELTA = DGEVAL(MSH,SPC,~,QQ) at the given barycentric nodes QQ. It is possible
%    to get the global points XG,YG as well by
% [DELTA,~,XG,YG] = DGEVAL(MSH,SPC,~,QQ), but these can also be obtained by
%    XG = DELTA*SPC.X (YG likewise; both blockform). With
% [~,UNVAL] = DGEVAL(MSH,SPC,UN,QQ) one gets the evaluation of UN at QQ.

% By W. Doerfler, KIT, 29.12.2018, dG 2.0.
%%

%% Analyse input
switch nargin
case 2
   un = [];
   qq = [];
case 3
   qq = [];
case 4
otherwise
   error('*** Check parameter list ***');
end

%% DIM=1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch msh.DIM
case 1

   if isempty(qq), qq = 1/2; end% Default is centre
   qq = 2*qq-1;% From [0,1] to [-1,1]
   Pq = Vandermonde1D(spc.pd,qq);% Evaluates modal basis in the given points
   Delta = Pq/spc.V0;% Evaluates nodal basis in the given points
   if ~isempty(un) && nargout>1
      if size(un,2)==1% un long form
         unval = Delta*reshape(un,[],msh.Ncell);% Evaluate in all cells
         unval = unval(:);% long form again
      else% un block form
         unval = Delta*un;% Evaluate in all cells
      end
   else
      unval = [];
   end
   if nargout>2
      xg = Delta*spc.x;% The global cellwise evaluation points
      yg = [];
   else
      xg = [];
      yg = [];
   end

%% DIM=2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
case 2

   if isempty(qq), qq = [1;1]/3; end% Default is center
   qq = [0 2;-2 -2]*qq+repmat([-1;1],1,size(qq,2));% 
   Pq = Vandermonde2D(spc.pd,qq(1,:)',qq(2,:)');% Evaluates modal basis in the given points
   Delta = Pq/spc.V0;% Evaluates nodal basis in the given points
   if ~isempty(un) && nargout>1
      if size(un,2)==1% un long form
         unval = Delta*reshape(un,[],msh.Ncell);% Evaluate in all cells
         unval = unval(:);% long form again
      else% un block form
         unval = Delta*un;% Evaluate in all cells
      end
   else
      unval = [];
   end
   if nargout>2
      xg = Delta*spc.x;% The global cellwise evaluation points: x-coordinate
      yg = Delta*spc.y;% The global cellwise evaluation points: y-coordinate
   else
      xg = [];
      yg = [];
   end

%% ELSE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
otherwise
   error('*** Not implemented ***');
end

%% END
