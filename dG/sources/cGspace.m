function spc = cGspace(msh,pd0)
%% CALL: spc = cGspace(msh,pd0);
% This is just the call
%    spc = cGspace(msh,pd0,opt);
% with opt.type = 'cG'. For further information see dGspace.

% W. Doerfler, KIT, 26.12.2017, dG 2.0.
%%

opt = struct('type','cG');
switch nargin
case 1
   spc = dGspace(msh,1,opt);
case {2,3}
   spc = dGspace(msh,pd0,opt);
otherwise
   error('*** Check input parameter ***');
end
