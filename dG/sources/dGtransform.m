function msh = dGtransform(msh,ctrans)
%% CALL: msh = dGtransform(msh,ctrans);
% INPUT:
%    msh ... STRUCT; (DG)Mesh-structure.
%    ctrans ... FUNC; Map IR^2->IR^2 [DEF []].
% OUTPUT:
%    msh ... STRUCT; (DG)Mesh-structure with transformed coordinates.
% DESCRIPTION:
% DGTRANSFORM applies a transformation to the mesh coordinates.
% MSH = DGTRANSFORM(GRD,CTRANS) transforms the mesh coordinates of MSH by a 
% transformation [[X,Y] |-> CTRANS([X,Y]) (here X,Y are columns).

% W. Doerfler, KIT, 06.03.2018, dG 2.0.
%%

switch msh.DIM
%% DIM=1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
case 1
   %% Analyse input data
   if nargin < 2
      return
   end
   if isempty(ctrans), return, end

   %% Transformation
   msh.vx = ctrans(msh.vx')';% vx is a row, ctrans works on columns

%% DIM=2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
case 2

   %% Analyse input data
   if nargin < 2
      return
   end
   if isempty(ctrans), return, end
  
   %% Transformation
   zz = ctrans([msh.vx',msh.vy']);% vx,vy are rows, ctrans works on columns
   msh.vx = zz(:,1)';
   msh.vy = zz(:,2)';
   if size(zz,2)==3
      msh.vz = zz(:,3)';
   end
otherwise
   error(' *** Error *** msh.DIM out of range');
end

%%
return
