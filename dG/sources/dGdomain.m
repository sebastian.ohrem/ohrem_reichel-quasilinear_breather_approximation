function msh = dGdomain(domain)
%% CALL: msh = dGdomain(domain);
% INPUT:
%    domain ... STRUCT; Domain description (see below, DEF see below).
% OUTPUT:
%    msh ... STRUCT; (DG)Mesh-structure.
% DESCRIPTION:
% MSH = DGDOMAIN(DOMAIN) Defines mesh structure MSH from domain information
%    given in the structure DOMAIN. For MSH see: dGmesh.m.
% MSH = DGDOMAIN() returns a default mesh structure.
% DOMAIN is a structure with the following attributes
%    tag ... Symbol of a predefined domain [DEF 'S'].
%    ncpd ... INT; Number of cells per dimension [DEF 4].
%    geom ... DOUBLE*; Centre and scale of the domain
%             [DEF [zeros(dim) 2*ones(dim)]].
%    filepath ... STRING; String of a pathname to read data.
%    prog ... STRING; Name of a program 'msh=prog(domain,...)'.
%    getbb ... FUN; Returns a bounding box 1x4 for the domain from geom.
%    trans ... FUN; Transforms the mesh vertices [x,y] -> [X,Y].
%    tstr ... STRING; Required triangulation type: criss, cross, criss-cross.
%    XTOL ... DOUBLE; A distance to identify when points are surely identical.
% Predefined tags (default if geom=[]):
%    'I'  - Interval (DIM=1, DEF [-1 1]),
%    'S'  - Square (DIM=2, DEF [-1 1]^2),
%    xxx 'To' - Square-Torus (DIM=2, DEF (0,1)^2), siehe fem-richter/extensions,
%    'R'  - Rectangle (DIM=2, DEF [-1 1]^2),
%    'C'  - Corner-domain (DIM=2, DEF in [-1 1]^2),
%    'L'  - L-domain (DIM=2, DEF in [-1 1]^2),
%    'T'  - Triangle (DIM=2, DEF in [-1 1]^2),
%    'D'  - Disc (DIM=2, DEF B_1(0)),
%    'Ds' - Disc-sector (DIM=2, DEF B_1(0) upper part),
%    'A'  - Annulus (DIM=2, DEF B_1(0)\B_{1/2}(0)),
%    'HR' - Holed rectangle (DIM=2, DEF [0,1]^2\[1/4,3/4]^2),
%    'K'  - Church (DIM=2),
%    'S2' - Sphere (DIM=2, manifold).
% Files: Some files are collected in grids/2D. See showDomain.m for examples.

% W. Doerfler, KIT, 24.05.2019, dG 2.0.
%%

%% Analyse input, set up problem
if nargin==0
   msh = defaultdomainoptions();
   return
elseif nargin==1
   ncpd = domain.ncpd;
   geom = domain.geom;
elseif nargin==2
else
   error('*** Check parameter list ***');
end

%% DIM=1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmp(domain.tag,'I')
   if isempty(geom), geom = [0 2]; end% [-1 1]
   xmin = geom(1)-geom(2)/2;
   xmax = geom(1)+geom(2)/2;
   % Number of vertices
   Nvert = ncpd+1;
   % Number of faces per cell (DIM!)
   Nfpc = 2;% Number of faces per cell
   % Generate global vertex coordinates
   vx = xmin+(xmax-xmin)*(0:ncpd)/ncpd;
   % The grid may be distortet
   vx(2:end-1) = vx(2:end-1)+domain.distort*(2*rand(1,Nvert-2)-1)/ncpd;
   % Boundary vertices
   bverts = [1,Nvert];
   % Set cells to vertex connectivity
   CtoV = [(1:Nvert-1)',(2:Nvert)'];
   % Set cells to element connectivity (neighbours)
   CtoC = [(0:ncpd-1)',(2:ncpd+1)'];
   CtoC(1,1) = -1; CtoC(end,2) = -1;% Nonexisting neigbours are -1
   % Set cells to neighbouring face connectivity
   CtoF = repmat([2,1],ncpd,1);
   % Set up structure and fill into msh structure
   msh = dGmesh(1,Nvert,ncpd,Nfpc,vx,[],[],CtoC,CtoF,CtoV,domain.XTOL,bverts);
   % Perform possible coordinate transformation
   msh = dGtransform(msh,domain.trans);
   return

%% DIM=2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif strcmp(domain.tag,'S')% Triangulated square (regular)
   if isempty(geom), geom = [0 0 2 2]; end% [-1 1]^2
   Nfpc = 3;% Number of faces per cell
   % Mesh from the fem-package
   fem_msh = femsquarem(ncpd,geom(3),domain.tstr);
   % Extract
   Nvert = size(fem_msh.verts,2);
   Ncell = size(fem_msh.faces,2);
   vx = fem_msh.verts(1,:);
   vy = fem_msh.verts(2,:);
   CtoV = fem_msh.faces';
   bverts = fem_msh.bverts(1,:);
   % Shift square
   vx = vx+geom(1)-geom(3)/2; vy = vy+geom(2)-geom(3)/2;
   % Build connectivity matrix
   [CtoC,CtoF] = Connect2D(CtoV);% To please original [HW] codes
      % Followed by a permutation if opposite positions are used, however, it is
      % in conflict with [HW]-routines. We use: First edge is the one right to
      % the 1. vertex, orientation is counter-clockwise.
      %CtoC = CtoC(:,[2 3 1]);% First triangle opposite first vertex
      %CtoF = CtoF(:,[2 3 1]);% Adjust
   % Fill into msh structure
   msh = dGmesh(2,Nvert,Ncell,Nfpc,vx,vy,[],CtoC,CtoF,CtoV,domain.XTOL,bverts);
elseif strcmp(domain.tag,'R')% Triangulated rectangle (regular)
   if isempty(geom), geom = [0 0 2 2]; end% [-1 1]^2
   Nfpc = 3;% Number of faces per cell
   if size(ncpd,2)==1, ncpd = [ncpd,ncpd]; end% Double a single entry
   % Mesh from the fem-package
   fem_msh = femrectm(ncpd(1),ncpd(2),geom(3),geom(4),domain.tstr);
   % Extract
   Nvert = size(fem_msh.verts,2);
   Ncell = size(fem_msh.faces,2);
   vx = fem_msh.verts(1,:);
   vy = fem_msh.verts(2,:);
   CtoV = fem_msh.faces';
   bverts = fem_msh.bverts(1,:);
   % Shift square
   vx = vx+geom(1)-geom(3)/2; vy = vy+geom(2)-geom(4)/2;
   % Build connectivity matrix
   [CtoC,CtoF] = Connect2D(CtoV);
   % Fill into msh structure
   msh = dGmesh(2,Nvert,Ncell,Nfpc,vx,vy,[],CtoC,CtoF,CtoV,domain.XTOL,bverts);
elseif strcmp(domain.tag,'C')% Triangulated corner domain (regular)
   if isempty(geom), geom = [0 0 2 2]; end% in [-1 1]^2
   Nfpc = 3;% Number of faces per cell
   % Mesh from the fem-package
   fem_msh = femcornerm(ceil(ncpd/2),geom(1),geom(2),geom(3)/2,domain.tstr);
   % Extract
   Nvert = size(fem_msh.verts,2);
   Ncell = size(fem_msh.faces,2);
   vx = fem_msh.verts(1,:);
   vy = fem_msh.verts(2,:);
   CtoV = fem_msh.faces';
   bverts = fem_msh.bverts(1,:);
   % Shift square
   %vx = vx+geom(1)-geom(3)/2; vy = vy+geom(2)-geom(4)/2;
   % Build connectivity matrix
   [CtoC,CtoF] = Connect2D(CtoV);
   % Fill into msh structure
   msh = dGmesh(2,Nvert,Ncell,Nfpc,vx,vy,[],CtoC,CtoF,CtoV,domain.XTOL,bverts);
elseif strcmp(domain.tag,'L')% Case L-shaped domain
   if isempty(geom), geom = [0 0 2 2]; end% in [-1 1]^2
   Nfpc = 3;% Number of faces per cell
   % Mesh from the fem-package
   fem_msh = femlshapem(ceil(ncpd/2),geom(1),geom(2),geom(3)/2,domain.tstr);
   fem_msh.verts(2,:) = 2*geom(2)-fem_msh.verts(2,:);% Flip at y-axis
   % Extract
   Nvert = size(fem_msh.verts,2);
   Ncell = size(fem_msh.faces,2);
   vx = fem_msh.verts(1,:);
   vy = fem_msh.verts(2,:);
   CtoV = fem_msh.faces';
   bverts = fem_msh.bverts(1,:);
   % Build connectivity matrix
   [CtoC,CtoF] = Connect2D(CtoV);
   % Fill into msh structure
   msh = dGmesh(2,Nvert,Ncell,Nfpc,vx,vy,[],CtoC,CtoF,CtoV,domain.XTOL,bverts);
elseif strcmp(domain.tag,'T')% Case triangular domain
   if isempty(geom), geom = [0 0 2 2]; end% in [-1 1]^2
   Nfpc = 3;% Number of faces per cell
   % Mesh from the fem-package
   L = geom(3); B = geom(4);% Domains size
   fem_msh = femrtriangm(ncpd,geom(1)-L/2,geom(2)-B/2,L,B,domain.tstr);
   % Extract
   Nvert = size(fem_msh.verts,2);
   Ncell = size(fem_msh.faces,2);
   vx = fem_msh.verts(1,:);
   vy = fem_msh.verts(2,:);
   CtoV = fem_msh.faces';
   bverts = fem_msh.bverts(1,:);
   % Build connectivity matrix
   [CtoC,CtoF] = Connect2D(CtoV);
   % Fill into msh structure
   msh = dGmesh(2,Nvert,Ncell,Nfpc,vx,vy,[],CtoC,CtoF,CtoV,domain.XTOL,bverts);
elseif strcmp(domain.tag,'D')% Case disc
   if isempty(geom), geom = [0 0 1 1]; end% B_1(0)
   Nfpc = 3;% Number of faces per cell
   % Mesh from the fem-package
   fem_msh = femdiscm(ceil(ncpd/2),geom(1),geom(2),geom(3),1);
   shv = geom(1:2);% Shift
   if norm(shv,2)>0, fem_msh = femtranslm(fem_msh,shv'); end% Translate if necessary
   % Extract
   Nvert = size(fem_msh.verts,2);
   Ncell = size(fem_msh.faces,2);
   vx = fem_msh.verts(1,:);
   vy = fem_msh.verts(2,:);
   CtoV = fem_msh.faces';
   bverts = fem_msh.bverts(1,:);
   % Build connectivity matrix
   [CtoC,CtoF] = Connect2D(CtoV);
   % Projection to the boundary
   bdpr = @(x1,x2) repmat(sqrt(x1(:,1).^2+x1(:,2).^2)./sqrt((x1(:,1)+x2(:,1)).^2+(x1(:,2)+x2(:,2)).^2),1,2).*(x1+x2);
   % Fill into msh structure
   msh = dGmesh(2,Nvert,Ncell,Nfpc,vx,vy,[],CtoC,CtoF,CtoV,domain.XTOL,bverts,bdpr);
elseif strcmp(domain.tag,'Ds')% Case disc-sector
   if isempty(geom), geom = [0 0 1 0 pi]; end% semicircle
   Nfpc = 3;% Number of faces per cell
   % Mesh from the fem-package
   fem_msh = femsectorm(ceil(ncpd/2),geom(1),geom(2),geom(3),geom(4),geom(5),1);
   shv = geom(1:2);% Shift
   if norm(shv,2)>0, fem_msh = femtranslm(fem_msh,shv'); end% Translate if necessary
   % Extract
   Nvert = size(fem_msh.verts,2);
   Ncell = size(fem_msh.faces,2);
   vx = fem_msh.verts(1,:);
   vy = fem_msh.verts(2,:);
   CtoV = fem_msh.faces';
   bverts = fem_msh.bverts(1,:);
   % Build connectivity matrix
   [CtoC,CtoF] = Connect2D(CtoV);
   % Fill into msh structure
   msh = dGmesh(2,Nvert,Ncell,Nfpc,vx,vy,[],CtoC,CtoF,CtoV,domain.XTOL,bverts);
elseif strcmp(domain.tag,'A')% Case annulus
   if isempty(geom), geom = [0 0 1/2 1]; end% B_1(0)\B_1/2(0)
   Nfpc = 3;% Number of faces per cell
   % Mesh from the fem-package
   fem_msh = femannulm(ceil(ncpd/2),geom(1),geom(2),geom(3),geom(4),domain.tstr);
   shv = geom(1:2);% Shift
   if norm(shv,2)>0, fem_msh = femtranslm(fem_msh,shv'); end% Translate if necessary
   % Extract
   Nvert = size(fem_msh.verts,2);
   Ncell = size(fem_msh.faces,2);
   vx = fem_msh.verts(1,:);
   vy = fem_msh.verts(2,:);
   CtoV = fem_msh.faces';
   bverts = fem_msh.bverts(1,:);
   % Build connectivity matrix
   [CtoC,CtoF] = Connect2D(CtoV);
   % Fill into msh structure
   msh = dGmesh(2,Nvert,Ncell,Nfpc,vx,vy,[],CtoC,CtoF,CtoV,domain.XTOL,bverts);
elseif strcmp(domain.tag,'HR')% Case holed rectangle
   if isempty(geom), geom = [0 0 2 2]; end% Q_2(0)\Q_1(0)
   Nfpc = 3;% Number of faces per cell
   % Mesh from the fem-package
   % ncpd should be a multiple of 4 for Q_2(0)\Q_1(0)
   ncpd = ncpd+4-mod(ncpd,4);% Change to next higher multiple of 4
   fem_msh = fmmrectinrect(ncpd,[geom(3),geom(4),geom(3)/2,geom(4)/2],domain.tstr);
   shv = geom(1:2)-[geom(3)/2,geom(4)/2];% Shift
   if norm(shv,2)>0, fem_msh = femtranslm(fem_msh,shv'); end% Translate if necessary
   % Extract
   Nvert = fem_msh.nv;
   Ncell = fem_msh.nt;
   vx = fem_msh.verts(1,:);
   vy = fem_msh.verts(2,:);
   CtoV = fem_msh.nodes;
   % Build connectivity matrix
   [CtoC,CtoF] = Connect2D(CtoV);
   % Fill into msh structure
   msh = dGmesh(2,Nvert,Ncell,Nfpc,vx,vy,[],CtoC,CtoF,CtoV,domain.XTOL);
elseif strcmp(domain.tag,'K')% Church example
   Nfpc = 3;% Number of faces per cell
   % Mesh from the the fem-package
   coord = load([domain.filepath '/church_coord.dat']); coord(:,1) = [];
   nodes = load([domain.filepath '/church_nodes.dat']); nodes(:,1) = [];
   %neigh = load([domain.filepath '/church_neigh.dat']); neigh(:,1) = [];
   % Extract
   Nvert = size(coord,1);
   Ncell = size(nodes,1);
   vx = coord(:,1)';
   vy = coord(:,2)';
   CtoV = nodes;
   % Build connectivity matrix
   [CtoC,CtoF] = Connect2D(CtoV);
   % Fill into msh structure
   msh = dGmesh(2,Nvert,Ncell,Nfpc,vx,vy,[],CtoC,CtoF,CtoV,domain.XTOL);
elseif strcmp(domain.tag,'S2')% Sphere (2 versions)
   Nfpc = 3;% Number of faces per cell
   coord = load([domain.filepath '/sphere_coord.dat']); coord(:,1) = [];
   nodes = load([domain.filepath '/sphere_nodes.dat']); nodes(:,1) = [];
   %neigh = load([domain.filepath '/sphere_neigh.dat']); neigh(:,1) = [];
   %coord = load([domain.filepath '/sphere_V5_coord.dat']); coord(:,1) = [];
   %nodes = load([domain.filepath '/sphere_V5_nodes.dat']); nodes(:,1) = [];
   %neigh = load([domain.filepath '/sphere_V5_neigh.dat']); neigh(:,1) = [];
   % Extract
   Nvert = size(coord,1);
   Ncell = size(nodes,1);
   vx = coord(:,1)';
   vy = coord(:,2)';
   vz = coord(:,3)';
   CtoV = nodes;
   % Build connectivity matrix
   [CtoC,CtoF] = Connect2D(CtoV);
   % Fill into msh structure
   msh = dGmesh(2,Nvert,Ncell,Nfpc,vx,vy,vz,CtoC,CtoF,CtoV,domain.XTOL);
   % REST
   %mark = m*ones(size(nodes,1),1);
   %[coord,nodes,neigh] = bisect(coord,nodes,neigh,mark,@bdryball);
   %for i=1:size(coord,1)
   %   coord(i,:) = coord(i,:)./norm(coord(i,:),2);
   %end;
else
   error('*** No such domain ***');
end

%% Perform possible coordinate transformation (d=2)
msh = dGtransform(msh,domain.trans);

%%
return

%% -----------------------------------------------------------------------------

function opt = defaultdomainoptions()

% Defaults
tag = 'S';% Domain tag: Square
ncpd = 4;% Number of cells per dimension
geom = [1/2 1/2 1 1];% [0,1]^2 (midpoint-xdiam,ydiam)
trans = [];% A coordinate transformation map z=trans([x,y]), []=id
tstr = 'criss-cross';% One of: criss, cross, criss-cross
getbb = @(g) getBoundingBox(g);% Get bounding box (see header below)

% Fill structure
opt = struct('tag',tag,'ncpd',ncpd,'geom',geom,'filepath',[],'prog',[], ...
         'trans',trans,'getbb',getbb,'tstr',tstr,'XTOL',1.0e-12,'distort',0);

% Remarks:
% o XTOL is a threshold to distinguish points. Warning: h_min>XTOL required!
% o An example for 'file' would be
%   domain.file = 'grids/2D/squarereg4_01.dat';% [0,1]^2 with criss-cross

return

%% -----------------------------------------------------------------------------

function bb = getBoundingBox(g)
% Computes a bounding box, considering a possible nonlinear coordinate
% transformation.
% If 'g' is 1x4 then the straight geom-box is assumed. If 'g' is a
% structure, then it is assumed to have components 'geom' and 'trans' at least
% (eg a 'domain' structure). 'trans' may be empty.

%% Analyse input
if nargin~=1, error('*** Check parameter list ***'); end
if isnumeric(g)
   trans = [];
   geom  = g;
elseif isstruct(g)
   trans = g.trans;
   geom  = g.geom;
else
   error('*** Check parameter list ***');
end

%% The straight box
bb = zeros(1,4);% Bounding box derived from geom
bb(1) = geom(1)-geom(3)/2; bb(2) = geom(1)+geom(3)/2;
bb(3) = geom(2)-geom(4)/2; bb(4) = geom(2)+geom(4)/2;

%% Use transformation info
if ~isempty(trans)
   tb = linspace(0,1,21)';
   nbb = bb;
   % Lower
   f12 = @(t) nbb(1)+t*(nbb(2)-nbb(1));
   bdryarc = trans([f12(tb),nbb(3)*ones(size(tb))]);
   bb(3) = min(bdryarc(:,2));
   % Upper
   bdryarc = trans([f12(tb),nbb(4)*ones(size(tb))]);
   bb(4) = max(bdryarc(:,2));
   % Left
   f34 = @(t) nbb(3)+t*(nbb(4)-nbb(3));
   bdryarc = trans([nbb(1)*ones(size(tb)),f34(tb)]);
   bb(1) = min(bdryarc(:,1));
   % Right
   bdryarc = trans([nbb(2)*ones(size(tb)),f34(tb)]);
   bb(2) = max(bdryarc(:,1)); 
end

return
