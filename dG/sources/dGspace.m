function spc = dGspace(msh,pd0,opt)
%% CALL: spc = dGspace(msh,pd0,opt);
% INPUT:
%    pd0 ... INT; Polynomial degree [DEF 1].
%    msh ... STRUCT; (DG)Mesh-structure.
%    opt ... STRUCT; Options (see below)
% OUTPUT:
%    spc ... STRUCT; (DG)Space-structure.
% DESCRIPTION:
% SPC = DGSPACE(MSH,PD0) generates a DG space structure SPC on the given mesh of
%    constant polynomial degree PD0.
% SPC = DGSPACE(MSH) is the same as SPC = DGSPACE(MSH,1).
% For the attributes of the structure SPC see the definition in the code.
% SPC = DGSPACE(MSH,PD0,OPTS) allows to insert options. 
% The attributes of OPTS are
%    type - {'dG','cG'}. Default is 'dG', 'cG' is set by calling 'cGspace'.

% Hesthaven/Warburton, Nodal Discontinuous Galerkin Methods, 2008.
% W. Doerfler, KIT, 06.01.2019, dG 2.0.
%%

%% Analyse input
if nargin<1
   error('*** Check input parameter ***');
elseif nargin<2
   pd = 1;
   opt = struct('type','dG');
elseif nargin<3
   pd = pd0;
   opt = struct('type','dG');
elseif nargin<4
   pd = pd0;
else
   error('*** Check input parameter ***');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Switch according to msh.DIM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch msh.DIM

%% 1D %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
case 1

%% Global numbers
Ncell = msh.Ncell;% Number of cells
Nfpc  = msh.Nfpc;% Number of faces per cell
Nnpc  = pd+1;% Number of nodes per cell
Nnpf  = 1;% Number of nodes per face
Nnode = Nnpc*Ncell;% Number of nodes
Nsnod = Ncell*Nfpc*Nnpf;% Number of skeleton nodes

%% Local mesh

% Compute basic Legendre-Gauss-Lobatto grid for degree pd
xi = JacobiGL(0,0,pd);

% Build reference element matrices
V0 = Vandermonde1D(pd,xi);
V1 = GradVandermonde1D(pd,xi);

% Set facemasks
fmask = [1,pd+1];% Face mask, first local node, last local node

%% Global mesh

% Build coordinates for all the nodes
ia = msh.CtoV(:,1)'; ib = msh.CtoV(:,2)';
x = ones(pd+1,1)*msh.vx(ia) + (xi+1)*(msh.vx(ib)-msh.vx(ia))/2;

% Set skeleton variables
nodes = reshape(1:Nnode,pd+1,Ncell);% Number volume nodes consecutively
snodesM = zeros(Nnpf,Nfpc,Ncell);% The skeleton nodes
for k=1:Ncell
   for f=1:Nfpc
      % Find index of face nodes with respect to volume node ordering
      snodesM(:,f,k) = nodes(fmask(:,f),k);
   end
end
snodesP = snodesM;
snodesP(:,1,:) = max(snodesM(:,1,:)-1,1);
snodesP(:,2,:) = min(snodesM(:,2,:)+1,Nnode);
snodesM = snodesM(:);
snodesP = snodesP(:);
% Get the reversed snodesP
snodesPrev = zeros(Nnode,1);
for i=1:Nsnod
   ns = snodesP(i);
   snodesPrev(ns) = i;
end
% Face to face skeleton node identification on skeleton index level
ftof = snodesPrev(snodesM);
% snodesM to snodesP at skeleton index level
EsP = speye(Nsnod);
EsP = EsP(:,ftof);% Now Esp*snodesM = snodesP
% Extension map: skeleton nodes to nodes (also snodes = Ens'*nodes)
Ens = speye(Nnode); Ens = Ens(:,snodesM);
% Skeleton coordinates
xs = x(fmask(:),:);% OR: xs(:) = x(snodesM), xs(:) = Ens'*x(:)

% Calculate geometric factors of the skeleton
[xi_x,J] = GeometricFactors1D(x,V1/V0);
% Calculate normals on the skeleton
nx = Normals1D(Nnpf,Nfpc,Ncell);
% Build inverse metric on the skeleton
facescale = 1./J(fmask,:);
% Cell geometry in a substructure
cgeom = struct('xi_x',xi_x,'J',J,'nx',nx,'facescale',facescale);

% Boundary skeleton nodes and coordinates
sbnodes = find(snodesM==snodesP);% bnodes = snodesM(sbnodes);
xsb = xs(sbnodes);% ?

% Embedding of the continuous space into the discontinuous space (if required)
if strcmp(opt.type,'cG')
   Ec = getEc(msh,Nnode,pd,snodesM,snodesP);
   Ncnod = size(Ec,2);% Number of continuous nodes
else
   Ec = [];
   Ncnod = [];
end

%% Set up space structure
spc = struct('x',x,'pd',pd,'Nnode',Nnode,'Ncnod',Ncnod, ...
             'Nnpc',Nnpc,'Nnpf',Nnpf,'Nsnod',Nsnod, ...
             'xi',xi,'V0',V0,'V1',V1, ...
             'fmask',fmask,'Ens',Ens,'EsP',EsP,'snodes',snodesM, ...
             'Ec',Ec,'xs',xs,'cgeom',cgeom,'sbnodes',sbnodes,'xsb',xsb, ...
             'type',opt.type,'this','space');

%% 2D %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
case 2

%% Global numbers
Ncell = msh.Ncell;% Number of cells
Nfpc  = msh.Nfpc;% Number of faces per cell
Nnpc  = (pd+1)*(pd+2)/2;% Number of nodes per cell (triangles)
Nnpf  = pd+1;% Number of nodes per face
Nnode = Nnpc*Ncell;% Number of nodes
Nsnod = Ncell*Nfpc*Nnpf;% Number of skeleton nodes (?)
NODETOL = 1e-12;% To identify points

%% Local mesh

% Compute basic (optimised) grid for degree pd
[x0,y0] = Nodes2D(pd);
[r,s]   = xytors(x0,y0);

% Build reference element matrices
V0 = Vandermonde2D(pd,r,s);
[Vr,Vs] = GradVandermonde2D(pd,r,s);

% Set facemasks
% Find all the nodes on all edges
fmask1 = find( abs(s+1) < NODETOL )'; 
fmask2 = find( abs(r+s) < NODETOL )';
fmask3 = find( abs(r+1) < NODETOL )';
fmask = [fmask1;fmask2;fliplr(fmask3)]';% Face mask
vmask = [1 pd+1 Nnpc];% Vertex mask
%Fx = x0(fmask(:),:);
%Fy = y0(fmask(:),:);

%% Global mesh

% Build coordinates for all the nodes
ia = msh.CtoV(:,1)'; ib = msh.CtoV(:,2)'; ic = msh.CtoV(:,3)';
x = (-(r+s)*msh.vx(ia)+(1+r)*msh.vx(ib)+(1+s)*msh.vx(ic))/2;
y = (-(r+s)*msh.vy(ia)+(1+r)*msh.vy(ib)+(1+s)*msh.vy(ic))/2;

% Set skeleton variables
%[mapM, mapP, vmapM, vmapP, vmapB, mapB] ... % in [HW]
[~, ~, snodesM, snodesP, ~,~] ...
   = BuildMaps2D(Ncell,Nnpc,Nnpf,Nfpc,fmask,msh.CtoC,msh.CtoF, ...
                 msh.CtoV,msh.vx,msh.vy,x,y);
% Extension map: skeleton nodes to nodes (gives snodesM = Ens'*nodes)
Ens = speye(Nnode,Nnode); Ens = Ens(:,snodesM);
% Skeleton coordinates
xs = x(fmask(:),:);% OR: xs(:) = x(snodesM), xs(:) = Ens'*x(:)
ys = y(fmask(:),:);% OR: ys(:) = y(snodesM), ys(:) = Ens'*y(:)

% Calculate geometric factors of the skeleton
[xi_x,J,x_xi] = GeometricFactors2D(x,y,Vr/V0,Vs/V0);
% Calculate normals on the skeleton
[nx,ny,sJ] = Normals2D(x_xi,fmask,Nnpf,Nfpc,Ncell);
% Build inverse metric on the skeleton
facescale = sJ./J(fmask,:);
% Cell geometry in a substructure
cgeom = struct('xi_x',xi_x,'J',J,'nx',nx,'ny',ny,'facescale',facescale);

% Boundary skeleton nodes and coordinates
sbnodes = find(snodesM==snodesP);
xsb = xs(sbnodes); 
ysb = ys(sbnodes);

% Embedding of the continuous space into the discontinuous space (if required)
if strcmp(opt.type,'cG')
   Ec = getEc(msh,Nnode,pd,snodesM,snodesP);
   Ncnod = size(Ec,2);% Number of continuous nodes
else
   Ec = [];
   Ncnod = [];
end

%% Set up space structure
spc = struct('x',x,'y',y,'pd',pd,'Nnode',Nnode,'Ncnod',Ncnod, ...
             'Nnpc',Nnpc,'Nnpf',Nnpf,'Nsnod',Nsnod, ...
             'r',r,'s',s,'V0',V0,'Vr',Vr,'Vs',Vs, ...
             'fmask',fmask,'vmask',vmask,'Ens',Ens,'EsP',[],'snodes',snodesM,'snodesP',snodesP, ...
             'Ec',Ec,'xs',xs,'ys',ys,'sbnodes',sbnodes,'xsb',xsb,'ysb',ysb, ...
             'cgeom',cgeom,'type',opt.type);

% Note: missing ftof, EsP (then snodesP avoidable)

% Some formulas:
% bnodes = snodesM(sbnodes);

%% Error %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
otherwise
   error('*** Not implemented')
end

%% END
end

%% Subroutines %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Ec = getEc(msh,Nnode,pd,snodes,snodesP)
%    Ec ... DOUBLE(Nnode,Ncnod);

%% Embeds the continuous space into the discontinuous space

% A speye for all nodes
Ec = speye(Nnode);

% Distinguish by DIM
switch msh.DIM
case 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Note the coupling over the cell border
%snodesP = spc.EsP*snodes;
% OLD version
%for i=1:length(snodes)
%   Ec(snodes(i),snodesP(i)) = 1;
%end
% NEW version 06.01.2019
Ec = max(Ec,sparse(snodes,snodesP,ones(length(snodes),1),Nnode,Nnode));
Ec(:,snodesP(snodesP>snodes)) = [];% Kill the larger node number

case 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Collect node numbers around a vertex
VtoN  = zeros(msh.Nvert,8);% Max 8 neighbours assumed
inote = zeros(msh.Nvert,1);% Count nodes around vertices
sn = reshape(snodes,[],msh.Ncell);% snodes in block form
sv = 1:(pd+1):size(snodes,1);% Extract vertices from skeleton
for cc=1:msh.Ncell
   iv = msh.CtoV(cc,1); inote(iv) = inote(iv)+1;
   VtoN(iv,inote(iv)) = sn(sv(1),cc);
   iv = msh.CtoV(cc,2); inote(iv) = inote(iv)+1;
   VtoN(iv,inote(iv)) = sn(sv(2),cc);
   iv = msh.CtoV(cc,3); inote(iv) = inote(iv)+1;
   VtoN(iv,inote(iv)) = sn(sv(3),cc);
end
VtoN(:,max(inote)+1:8) = [];% Kill superfluous columns

% Add additional ones in Ec (vertices)
% OLD version
% for i=1:msh.Nvert
%    k = VtoN(i,1);% Assumption: The first is the smallest!
%    for j=2:size(VtoN,2)
%       if VtoN(i,j)>0
%          Ec(VtoN(i,j),k) = 1;
%       end
%    end
% end
% NEW version 06.01.2019
ii = zeros(8*Nnode,1); ij = zeros(8*Nnode,1); ss = zeros(8*Nnode,1);
ic = 0;
for i=1:msh.Nvert
   k = VtoN(i,1);% Assumption: The first is the smallest!
   for j=2:size(VtoN,2)
      if VtoN(i,j)>0
         ic = ic+1;
         ii(ic) = VtoN(i,j);
         ij(ic) = k;
         ss(ic) = 1;
      end
   end
end
ii(ic+1:end) = []; ij(ic+1:end) = []; ss(ic+1:end) = [];
Ec = Ec+sparse(ii,ij,ss,Nnode,Nnode);
%
VtoN(:,1) = [];% Remove first column
VtoN = sort(VtoN(:));% Sort the rest
VtoN(VtoN==0) = [];% Kill zeros
nkill = VtoN';

% Add additional ones in Ec (go around skeleton and fix interior edges)
if pd>1
   if pd==2,     fkill = [1 3 4 6 7 9]';% p=2
   elseif pd==3, fkill = [1 4 5 8 9 12]';% p=3
   elseif pd==4, fkill = [1 5 6 10 11 15]';% p=4
   else,  fkill = [1 pd+1 pd+2 2*pd+2 2*pd+3 3*(pd+1)]';% p>4
   end
   sn(fkill,:) = [];
   snp = reshape(snodesP,[],msh.Ncell);% EsP missing
   snp(fkill,:) = [];
   % OLD version
   %sn = sn(:); snp = snp(:);
   %for i=1:length(sn)
   %   Ec(sn(i),snp(i)) = 1;
   %end
   % NEW version 06.01.2019
   Ec = max(Ec,sparse(sn(:),snp(:),ones(size(sn(:))),Nnode,Nnode));
   nkill = sort([nkill,snp(snp>sn)']);
end

% Collect nodes to remove
Ec(:,nkill) = [];

otherwise
   error('*** Not implemented ***');
end

end