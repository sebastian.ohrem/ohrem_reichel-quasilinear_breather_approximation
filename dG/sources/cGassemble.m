function [A,B,C,F,M] = cGassemble(msh,spc,pde)
%% CALL: [A,B,C,F,M] = cGassemble(msh,spc,pde);
% INPUT:
%    msh ... STRUCT; (DG)Mesh-structure.
%    spc ... STRUCT; (CG)Space-structure.
%    pde ... STRUCT; PDE coefficients.
% OUTPUT:
%    A,B,C,M ... spDBLE(*,*); Matrix as below.
%    F ... spDBLE(*,1); Right hand side vector as below.
% DESCRIPTION:
% CGASSEMBLE establishes the matrices of the continuous Finite Element
% method for chosen mesh and space and pde-coefficients.
% [A,B,C,F,M] = CGASSEMBLE(MSH,SPC,PDE) provides matrices/vectors
%    A = [ \int a d\Psi_l d\Psi_k ]_kl
%    B = [ \int b d\Psi_l  \Psi_k ]_kl,
%    C = [ \int c  \Psi_l  \Psi_k ]_kl,
%    F = [ \int f \Psi_k - g d\Psi_k ]_k,
%    M = [ \int   \Psi_l \Psi_k ]_kl,
% for all basis elements {\Psi_l}_l.
% [A,B,C,F] = CGASSEMBLE(MSH,SPC,PDE) waives to compute M, correspondingly
% for left hand sides [A], [A,B], [A,B,C].

% Literature: Hesthaven/Warburton 2008.
% W. Doerfler, KIT, 02.02.2020, dG 2.0.
%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Switch according to msh.DIM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch msh.DIM

%% 1D %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
case 1

%% Prepare storage
nn  = spc.Nnode;% No of nodes (dG)
nnc = spc.Ncnod;% No of nodes (cG)
% Sparse indeces
m = spc.Nnpc; m2 = m*m;
pq = repmat(1:m,m,1); pp = pq';
ip = pp(:); iq = pq(:);
ki = zeros(msh.Ncell*m2,1);
li = zeros(msh.Ncell*m2,1);
icm = 0; idm = 0;
for k=1:msh.Ncell
   ki(idm+1:idm+m2) = icm+ip;% Storing positions
   li(idm+1:idm+m2) = icm+iq;
   icm = icm+m; idm = idm+m2;
end

% Get the local matrices
[M0,D,~,~] = dGassemloc(msh,spc);

% Local Jacobians
h = diff(msh.vx)/2;% h/2 actually

% Choose method to compute matrices for variable data
if pde.fstat.c==2||pde.fstat.b==2||pde.fstat.a==2 
   qstyle = 'quadrature';%
   if strcmp(qstyle,'quadrature')% Quadrature
      ord = 2*spc.pd;% Quadrature order (least reasonable value)
      qno = qruleGL(ord);% Quadrature nodes in [0,1]
      Lq = Vandermonde1D(spc.pd,2*qno-1)/spc.V0;% Evaluate nodal basis in q
      ia = msh.CtoV(:,1)'; ib = msh.CtoV(:,2)';% Initial and end points
      xq = (1-qno)*msh.vx(ia)+qno*msh.vx(ib);% Global quadrature points
   end
end
      
%% Diffusion coefficient
if nargout>0
   switch pde.fstat.a
   case 0% 'a' 0
      A = sparse(nnc,nnc);
   case 1% 'a' constant
      DD = pde.fun_a*D'*M0*D;% Local stiffness
      A = spc.Ec'*dGblockdiag(DD,msh.Ncell,1./h)*spc.Ec;% Contract to conforming space
   case 2% 'a' variable
      ss  = zeros(msh.Ncell*m2,1);
      idm = 0;
      if strcmp(qstyle,'quadrature')% Quadrature
         av = pde.fun_a(xq);% a-values
         for k=1:msh.Ncell
            Ma = 2*qruleGL(ord,av(:,k),Lq,Lq);% Weight 2!
            DD = D'*Ma*D;% Local stiffness matrix
            ss(idm+1:idm+m2) = DD(:)/h(k);% Store local matrix into global
            idm = idm+m2;
         end
      else
         error('*** Not implemented ***');
      end
      A = spc.Ec'*sparse(ki,li,ss,msh.Ncell*m,msh.Ncell*m)*spc.Ec;% Contract to conforming space
   end
else
   A = [];
end

%% Transport coefficient
if nargout>1
   switch pde.fstat.b
   case 0% 'b' 0
      B = sparse(nnc,nnc);
   case 1% 'b' constant
      Mb = pde.fun_b*M0*D;% Local transport
      B = spc.Ec'*dGblockdiag(Mb,msh.Ncell)*spc.Ec;% Contract to conforming space
   case 2% 'b' variable
      ss  = zeros(msh.Ncell*m2,1);
      idm = 0;
      if strcmp(qstyle,'quadrature')% Quadrature
         bv = pde.fun_b(xq);% b-values
         for k=1:msh.Ncell
            Mb = 2*qruleGL(ord,bv(:,k),Lq,Lq);% Weight 2!
            DD = Mb*D;% Local stiffness matrix
            ss(idm+1:idm+m2) = DD(:);% Store local matrix into global
            idm = idm+m2;
         end
      else
         error('*** Not implemented ***');
      end
      B = spc.Ec'*sparse(ki,li,ss,msh.Ncell*m,msh.Ncell*m)*spc.Ec;% Contract to conforming space
   end
else
   B = [];
end

%% Reaction coefficient
if nargout>2
   switch pde.fstat.c
   case 0% 'c' 0
      C = sparse(nnc,nnc);
   case 1% 'c' constant
      Mc = pde.fun_c*M0;% Local reaction
      C = spc.Ec'*dGblockdiag(Mc,msh.Ncell,h)*spc.Ec;% Contract to conforming space
   case 2% 'c' variable
      ss  = zeros(msh.Ncell*m2,1);
      idm = 0;
      if strcmp(qstyle,'quadrature')% Quadrature
         cv = pde.fun_c(xq);% c-values
         for k=1:msh.Ncell
            Mc = 2*qruleGL(ord,cv(:,k),Lq,Lq);% Weight 2
            ss(idm+1:idm+m2) = h(k)*Mc(:);% Store local matrix into global
            idm = idm+m2;
         end
      else
         error('*** Not implemented ***');
      end
      C = spc.Ec'*sparse(ki,li,ss,msh.Ncell*m,msh.Ncell*m)*spc.Ec;% Contract to conforming space
   end
else
   C = [];
end

%% Set up the global mass matrix
if nargout>3
   Mblock = dGblockdiag(M0,msh.Ncell,h);% Collect local mass into global block
   M = spc.Ec'*Mblock*spc.Ec;% Contract to conforming space
else
   M = [];
end

%% Set up rhs
if nargout>3
   switch pde.fstat.f
   case 0, F = sparse(nnc,1);
   case 1, F = pde.fun_f*spc.Ec'*Mblock*ones(nn,1);
   case 2, F = spc.Ec'*Mblock*pde.fun_f(spc.x(:));
   end
else
   F = [];
end

%% 2D %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
case 2

%% Prepare storage
nn  = spc.Nnode;% No of nodes (dG)
nnc = spc.Ncnod;% No of nodes (cG)
% Sparse indeces
m = spc.Nnpc; m2 = m*m;
pq = repmat(1:m,m,1); pp = pq';
ip = pp(:); iq = pq(:);
ki = zeros(msh.Ncell*m2,1);
li = zeros(msh.Ncell*m2,1);
icm = 0; idm = 0;
for k=1:msh.Ncell
    ki(idm+1:idm+m2) = icm+ip;% Storing positions
    li(idm+1:idm+m2) = icm+iq;
    icm = icm+m; idm = idm+m2;
end

% Get the local matrices (constant pd)
[M0,Dr,Ds,~] = dGassemloc(msh,spc);

% Local jacobians
vol = spc.cgeom.J(1,:)';% Constant for straight elements

% Choose method to compute matrices for variable data
if pde.fstat.c==2||pde.fstat.b==2||pde.fstat.a==2 
   qstyle = 'quadrature';%
   if strcmp(qstyle,'quadrature')% Quadrature
      ord = 2*spc.pd;% Quadrature order (least reasonable value)
      [qp,qw] = qrule2D(ord);% Get q-points and weights
      leq = length(qw);% Number of quadrature points
      rsq = 2*qp(:,1:2)-1;% The q-nodes in r,s coordinates
      Lq = Vandermonde2D(spc.pd,rsq(:,1),rsq(:,2))/spc.V0;% Eval nodal basis in q
      ia = msh.CtoV(:,1)'; ib = msh.CtoV(:,2)'; ic = msh.CtoV(:,3)';
      xq = (qp(:,3)*msh.vx(ia)+qp(:,1)*msh.vx(ib)+qp(:,2)*msh.vx(ic));% !
      yq = (qp(:,3)*msh.vy(ia)+qp(:,1)*msh.vy(ib)+qp(:,2)*msh.vy(ic));% !
   end
end

%% Diffusion coefficient
if nargout>0
   switch pde.fstat.a
   case 0% 'a' 0
      A = sparse(nnc,nnc);
   case 1% 'a' constant
      DDrr = Dr'*M0*Dr;% Local stiffness matrices
      DDrs = Dr'*M0*Ds;
      DDsr = Ds'*M0*Dr;
      DDss = Ds'*M0*Ds;
      % Set up the global block matrix
      ss = zeros(msh.Ncell*m2,1);
      idm = 0;
      for k=1:msh.Ncell
         rx = spc.cgeom.xi_x.rx(1,k);% Read [HW; p. 67]
         ry = spc.cgeom.xi_x.ry(1,k);
         sx = spc.cgeom.xi_x.sx(1,k);
         sy = spc.cgeom.xi_x.sy(1,k);
         volk = vol(k);% Constant for straight elements
         DD =  (rx*rx+ry*ry)*DDrr + (rx*sx+ry*sy)*(DDrs+DDsr) ...
             + (sx*sx+sy*sy)*DDss;% Local DD
         ss(idm+1:idm+m2) = volk*DD(:);% Store local matrix into global
         idm = idm+m2;
      end
      A = pde.fun_a ...
          *(spc.Ec'*sparse(ki,li,ss,msh.Ncell*m,msh.Ncell*m)*spc.Ec);% Reduce global block
   case 2% 'a' variable
      ss  = zeros(msh.Ncell*m2,1);
      idm = 0;
      at = pde.fun_a([spc.x(1,1),spc.y(1,1)]);% Test a-value
      a_is_scalar = 1; jcol = size(at,2);
      if jcol>1, a_is_scalar = 0; end
      if a_is_scalar
      if strcmp(qstyle,'quadrature')% Quadrature
         % Set up the global block matrix
         for k=1:msh.Ncell
            av = pde.fun_a([xq(:,k),yq(:,k)]);% a-values
            Ma = Lq'*diag(qw.*av)*Lq;
            DDrr = Dr'*Ma*Dr;% Local stiffnesses
            DDrs = Dr'*Ma*Ds;
            DDsr = Ds'*Ma*Dr;
            DDss = Ds'*Ma*Ds;
            rx = spc.cgeom.xi_x.rx(1,k);% Read [HW; p. 67]
            ry = spc.cgeom.xi_x.ry(1,k);
            sx = spc.cgeom.xi_x.sx(1,k);
            sy = spc.cgeom.xi_x.sy(1,k);
            volk = 2*vol(k);% Constant for straight elements; weight 2
            DD =  (rx*rx+ry*ry)*DDrr + (rx*sx+ry*sy)*(DDrs+DDsr) ...
                + (sx*sx+sy*sy)*DDss;% Local DD
            ss(idm+1:idm+m2) = volk*DD(:);% Store local matrix into global
            idm = idm+m2;
         end
      elseif strcmp(qstyle,'productrule')%
         % Set up the global block matrix
         for k=1:msh.Ncell
            av = pde.fun_a([spc.x(:,k),spc.y(:,k)]);% a-values
            da = diag(av);
            Ma = M0*da;% Only this works!
            DDrr = Dr'*Ma*Dr;% Local stiffnesses
            DDrs = Dr'*Ma*Ds;
            DDsr = Ds'*Ma*Dr;
            DDss = Ds'*Ma*Ds;
            rx = spc.cgeom.xi_x.rx(1,k);% Read [HW; p. 67]
            ry = spc.cgeom.xi_x.ry(1,k);
            sx = spc.cgeom.xi_x.sx(1,k);
            sy = spc.cgeom.xi_x.sy(1,k);
            volk = vol(k);% Constant for straight elements
            DD =  (rx*rx+ry*ry)*DDrr + (rx*sx+ry*sy)*(DDrs+DDsr) ...
                + (sx*sx+sy*sy)*DDss;% Local DD
            ss(idm+1:idm+m2) = volk*DD(:);% Store local matrix into global
            idm = idm+m2;
         end
      end
      else% a is matrix-valued
      qstyle = 'quadrature';%'productrule';%
      if strcmp(qstyle,'quadrature')% Quadrature
         av = pde.fun_a([xq(:),yq(:)]);% a-values
         switch jcol
         case 2, get_av = @(id) [av(id,1) 0; 0 av(id,2)];% Diagonal
         case 3, get_av = @(id) [av(id,1) av(id,2); av(id,2) av(id,3)];% Symmetric
         case 4, get_av = @(id) [av(id,1) av(id,2); av(id,3) av(id,4)];% Generell
         end
         % Set up the global block matrix
         for k=1:msh.Ncell
            % F transformation matrix (jacobian), assumed constant
            Fpinv = [spc.cgeom.xi_x.rx(1,k) spc.cgeom.xi_x.ry(1,k); ...
                     spc.cgeom.xi_x.sx(1,k) spc.cgeom.xi_x.sy(1,k)];
            bm = zeros(leq,2,2);
            for ll=1:leq
               bm(ll,:,:) = Fpinv*get_av((k-1)*leq+ll)*Fpinv';
            end
            Ma11 = Lq'*diag(qw.*bm(:,1,1))*Lq;
            Ma12 = Lq'*diag(qw.*bm(:,1,2))*Lq;
            Ma21 = Lq'*diag(qw.*bm(:,2,1))*Lq;
            Ma22 = Lq'*diag(qw.*bm(:,2,2))*Lq;
            DD =  Dr'*Ma11*Dr + Dr'*Ma12*Ds ...
                + Ds'*Ma21*Dr + Ds'*Ma22*Ds;
            volk = 2*vol(k);% Constant for straight elements, weight 2
            ss(idm+1:idm+m2) = volk*DD(:);% Store local matrix into global
            idm = idm+m2;
         end
      elseif strcmp(qstyle,'productrule')%
         % Set up the global block matrix
         for k=1:msh.Ncell
            % F transformation matrix (jacobian), assumed constant
            Fpinv = [spc.cgeom.xi_x.rx(1,k) spc.cgeom.xi_x.ry(1,k); ...
                     spc.cgeom.xi_x.sx(1,k) spc.cgeom.xi_x.sy(1,k)];
            bm = zeros(spc.Nnpc,2,2);
            for ll=1:spc.Nnpc
               av = pde.fun_a([spc.x(ll,k),spc.y(ll,k)]);% a-values
               bm(ll,:,:) = Fpinv*av*Fpinv';
            end
            MDr = M0*Dr; MDs = M0*Ds;
            DD =  MDr'*diag(bm(:,1,1))*Dr + MDr'*diag(bm(:,1,2))*Ds ...
                + MDs'*diag(bm(:,2,1))*Dr + MDs'*diag(bm(:,2,2))*Ds;
            volk = vol(k);% Constant for straight elements
            ss(idm+1:idm+m2) = volk*DD(:);% Store local matrix into global
            idm = idm+m2;
         end
      end
      end% if quadrature
      A = spc.Ec'*sparse(ki,li,ss,msh.Ncell*m,msh.Ncell*m)*spc.Ec;% Reduce global block
   end
else
   A = [];
end

%% Transport coefficient
if nargout>1
   switch pde.fstat.b
   case 0% 'b' 0
      B = sparse(nnc,nnc);
   case 1% 'b' constant
      ss  = zeros(msh.Ncell*m2,1);
      idm = 0;
      MDr = M0*Dr;
      MDs = M0*Ds;
      for k=1:msh.Ncell
         rx = spc.cgeom.xi_x.rx(1,k);% Read [HW; p. 67]
         ry = spc.cgeom.xi_x.ry(1,k);
         sx = spc.cgeom.xi_x.sx(1,k);
         sy = spc.cgeom.xi_x.sy(1,k);
         volk = vol(k);% Constant for straight elements
         B1 = pde.fun_b(1)*(rx*MDr+sx*MDs)*volk;
         B2 = pde.fun_b(2)*(ry*MDr+sy*MDs)*volk;
         ss(idm+1:idm+m2) = B1(:)+B2(:);% Store local matrix into global
         idm = idm+m2;
      end
      B = spc.Ec'*sparse(ki,li,ss,msh.Ncell*m,msh.Ncell*m)*spc.Ec;% Contract to conforming space
   case 2% 'b' variable
      ss  = zeros(msh.Ncell*m2,1);
      idm = 0;
      qstyle = 'quadrature';%'productrule';%
      if strcmp(qstyle,'quadrature')% Quadrature
      bv = pde.fun_b([xq(:),yq(:)]);% b-values
      for k=1:msh.Ncell
         bvl = bv((k-1)*leq+(1:leq),:);% Local b-values
         Mb1 = Lq'*diag(qw.*bvl(:,1))*Lq;
         Mb2 = Lq'*diag(qw.*bvl(:,2))*Lq;
         rx = spc.cgeom.xi_x.rx(1,k);% Read [HW; p. 67]
         ry = spc.cgeom.xi_x.ry(1,k);
         sx = spc.cgeom.xi_x.sx(1,k);
         sy = spc.cgeom.xi_x.sy(1,k);
         volk = 2*vol(k);% Constant for straight elements, weight 2
         B1 = Mb1*(rx*Dr+sx*Ds)*volk;
         B2 = Mb2*(ry*Dr+sy*Ds)*volk;
         ss(idm+1:idm+m2) = B1(:)+B2(:);% Store local matrix into global
         idm = idm+m2;
      end
      elseif strcmp(qstyle,'productrule')%
      for k=1:msh.Ncell
         bv = pde.fun_b([spc.x(:,k),spc.y(:,k)]);% b-values
         db1 = diag(bv(:,1)); db2 = diag(bv(:,2));
         rx = spc.cgeom.xi_x.rx(1,k);% Read [HW; p. 67]
         ry = spc.cgeom.xi_x.ry(1,k);
         sx = spc.cgeom.xi_x.sx(1,k);
         sy = spc.cgeom.xi_x.sy(1,k);
         volk = vol(k);% Constant for straight elements
         B1 = M0*db1*(rx*Dr+sx*Ds)*volk;
         B2 = M0*db2*(ry*Dr+sy*Ds)*volk;
         ss(idm+1:idm+m2) = B1(:)+B2(:);% Store local matrix into global
         idm = idm+m2;
      end
      end% if quadrature
      B = spc.Ec'*sparse(ki,li,ss,msh.Ncell*m,msh.Ncell*m)*spc.Ec;% Contract to conforming space
   end
else
   B = [];
end

%% Reaction coefficient
if nargout>2
   switch pde.fstat.c
   case 0% 'c' 0
      C = sparse(nnc,nnc);
   case 1% 'c' constant
      Mc = pde.fun_c*M0;% Local reaction
      C = spc.Ec'*dGblockdiag(Mc,msh.Ncell,vol)*spc.Ec;% Contract to conforming space
   case 2% 'c' variable
      qstyle = 'quadrature';%'productrule';%
      if strcmp(qstyle,'quadrature')% Quadrature.
         if spc.pd<2% Here c piecewise const only
            xc = dGmeshpoints(msh);
            cv = pde.fun_c(xc);% c-values
            C = spc.Ec'*dGblockdiag(M0,msh.Ncell,cv.*vol)*spc.Ec;% Contract to conforming space
         elseif spc.pd<10% Limit for implemented q-rules
            ss = zeros(msh.Ncell*m2,1);
            idm = 0;
            cv = pde.fun_c([xq(:),yq(:)]);% c-values
            for k=1:msh.Ncell
               Mc = 2*Lq'*diag(qw.*cv((k-1)*leq+(1:leq)))*Lq;% Weight 2
               ss(idm+1:idm+m2) = vol(k)*Mc(:);% Store local matrix into global
               idm = idm+m2;
            end
            C = spc.Ec'*sparse(ki,li,ss,msh.Ncell*m,msh.Ncell*m)*spc.Ec;% Contract to conforming space
         else
            error('*** Not yet implemented ***');
         end
      elseif strcmp(qstyle,'productrule')%
         ss = zeros(msh.Ncell*m2,1);
         idm = 0;
         for k=1:msh.Ncell
            cv = pde.fun_c([spc.x(:,k),spc.y(:,k)]);% c-values
            dc = diag(cv);
            Mc = M0*dc;% Only this works!
            ss(idm+1:idm+m2) = vol(k)*Mc(:);% Store local matrix into global
            idm = idm+m2;
         end
         C = spc.Ec'*sparse(ki,li,ss,msh.Ncell*m,msh.Ncell*m)*spc.Ec;% Contract to conforming space
      end
   end
else
   C = [];
end

%% Set up the global mass matrix
if nargout>3
   Mblock = dGblockdiag(M0,msh.Ncell,vol);% Collect local mass into global block
   M = spc.Ec'*Mblock*spc.Ec;% Contract to conforming space
else
   M = [];
end

%% Set up rhs
if nargout>3
   switch pde.fstat.f
   case 0, F = sparse(nnc,1);
   case 1, F = pde.fun_f*spc.Ec'*Mblock*ones(nn,1);
   case 2, F = spc.Ec'*Mblock*pde.fun_f([spc.x(:),spc.y(:)]);
   end
end

%% Error %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
otherwise
   error('*** Not implemented')
end

return
