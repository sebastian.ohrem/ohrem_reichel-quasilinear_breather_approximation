function msh = dGmesh(DIM,Nvert,Ncell,Nfpc,vx,vy,vz,CtoC,CtoF,CtoV,XTOL,bverts,bdpr)
%% CALL: msh = dGmesh(DIM,Nvert,Ncell,Nfpc,vx,vy,vz,CtoC,CtoF,CtoV,XTOL,bverts,bdpr);
% INPUT:
%    DIM ... DOUBLE; Spatial dimension.
%    Nvert ... INT; Number of vertices.
%    Ncell ... INT; Number of cells.
%    Nfpc ... INT; Number of faces per cell.
%    vx/vy/vz ... DOUBLE; x/y/z-coordinates of the vertices.
%    CtoC ... INT; To each cell the neighbouring cell [DEF []].
%    CtoF ... INT; To each cell the face ...  [DEF []].
%    CtoV ... INT; To each cell the vertices [DEF []].
%    XTOL ... DOUBLE; A length to identify when points are surely identical [DEF 10*eps].
%    bverts ... INT; Enlists boundary vertices [DEF []].
%    bdpr ... FUNC; Boundary projection [x1,x2]->proj((x1+x2)/2) [DEF []].
% OUTPUT:
%    msh ... STRUCT; (DG)Mesh-structure.
% DESCRIPTION:
% msh = dGmesh(DIM,Nvert,Ncell,Nfpc,vx,vy,CtoC,CtoF,CtoV,bverts) generates a
%    DG/FEM-mesh of Ncell cells.

% W. Doerfler, KIT, 09.08.2018, dG 2.0.
%%

%% Analyse input
if nargin<7
   error('*** Not enough input parameter ***');
end
if nargin<8
   CtoC = [];  CtoF = [];  CtoV = []; XTOL = 10*eps; bverts = []; bdpr = [];
elseif nargin<9
   CtoF = [];  CtoV = []; XTOL = 10*eps; bverts = []; bdpr = [];
elseif nargin<10
   CtoV = []; XTOL = 10*eps; bverts = []; bdpr = [];
elseif nargin<11
   XTOL = 10*eps; bverts = []; bdpr = [];
elseif nargin<12
   bverts = []; bdpr = [];
elseif nargin<13
   bdpr = [];
end

%% Create mesh-structure
msh.DIM   = DIM;%int8(DIM);
msh.Nvert = Nvert;%int32(Nvert);% Seems to create compatibility problems
msh.Ncell = Ncell;%int32(Ncell);
msh.Nfpc  = Nfpc;%int32(Nfpc);
msh.CtoC  = CtoC;%int32(CtoC);
msh.CtoF  = CtoF;%int32(CtoF);
msh.CtoV  = CtoV;%int32(CtoV);
msh.vx    = vx;
if DIM>1, msh.vy = vy; else msh.vy = []; end
msh.vz    = vz;
msh.XTOL  = XTOL;
if ~isempty(bverts)
   msh.bverts = bverts;
else
   msh.bverts = dGgetbverts(Ncell,Nvert,CtoC,CtoV);
end
msh.bdpr = bdpr;
msh.this = 'mesh';% Knows what it is

return

function bverts = dGgetbverts(Ncell,Nvert,CtoC,CtoV)

bmark = zeros(1,Nvert);
izyk = [1 2 3 1 2 3];
for tnr=1:Ncell
   for ii=1:3
      if CtoC(tnr,ii)==tnr% Case of a boundary triangle
         bmark(CtoV(tnr,izyk(ii:ii+1))) = 1;
      end
   end
end
bverts = 1:Nvert;% All verts
bverts(bmark==0) = [];% Kill inner points

return