xmax = c.R + iif(strcmp(potential, 'periodicstep'), 10, 6);
x_plot = linspace(0, xmax, 3000)';

timeorder = 1;
name = [getwsym(timeorder), ' ', dataname];
[~] = mkdir(getsavespath(name, []));
name = [name, filesep, 'img'];