% finding good numerical minimizers depends 
% on initial data for this problem
% Here are some other seeds that yield good solutions:
% R = 2 => 1179358219
% R = 2.15 => 1179676264

run data_defaults

fct_modifyc = @modify;
seed = 1179667836;
rng(seed);
fct_startingpoint = @randomstart;

run get_minimizer

function c = modify(c)
	c.R = 2.15;
	c.K = 8;
	c.numcells = 32;
end