plotting_fig = gcf().Number;
for pb = problems()
	[model, potential, nonlinearity, symmetry] = pb{:};
	
	clearvars -except model potential nonlinearity symmetry plotting_fig


	% get minimizer
	minscript = ['min', '_', model, '_', potential, '_', nonlinearity];
	if exist(minscript, 'file') ~= 0
		run(minscript);
	else
		run data_defaults.m
		run get_minimizer.m
	end

	% run breather_info.m

	% save image
	savescript = ['save', '_', model, '_', potential, '_', nonlinearity];
	if exist(savescript, 'file') ~= 0
		run(savescript);
	else
		figure(plotting_fig);
		run save_image_defaults.m
		run save_image.m

		% run save_video_defaults.m
		% run save_video.m

		% run save_frames_defaults.m
		% run save_frames.m
	end
end