% REQUIRES
%	model 
%	potential 
%	nonlinearity 
%	symmetry
% DESCRIPTION
%	generates default data for use with get_minimizer

dataname = [model, ',', potential, ',', nonlinearity, ',', symmetry];
modifyc = @(c) c;
numcells = 128;
K = 64;
withgradient = true;
