% finding good numerical minimizers depends 
% on initial data for this problem
% Here are some other seeds that yield good solutions:
% R = 2.15 => 1179687765
% R = 2.25 => 1179730038
% always rho = R + 1

run data_defaults

fct_modifyc = @modify;
seed = 1179449059;
rng(seed);
fct_startingpoint = @randomstart;

run get_minimizer

function c = modify(c)
	c.R = 2.25;
	c.K = 8;
	c.numcells = 32;
end