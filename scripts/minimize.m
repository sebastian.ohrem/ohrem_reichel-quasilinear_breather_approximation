function [hatu, c] = minimize(hatu, c, numcells, K, withgradient, fct_preferaddcells, fct_timedim)
	% DESCRIPTION
	%	attempts to minimize E using fminunc and repeated dimension doubling
	%	target dimensions are numcells (space) and K (time)

	format = '% .8f';

	% total number of iterations (1 + #doubling steps)
	numiterations = 1 + ceil(log2(numcells / c.numcells)) + ceil(log2(K / c.K));
	
	disp('Minimizing functional E ...');
	clf 
	
	function y = evalEsub(r, c)
		hatu_ = realtocoeff(r);
		y = evalE(hatu_, c);
	end
	function [y, gradr] = evalGradEsub(r, c)
		hatu_ = realtocoeff(r);
		y = evalE(hatu_, c);
		grad_ = evalGradE(hatu_, c);
		gradr = coefftoreal(grad_);
	end

	options = optimoptions('fminunc', ...
		'Display', 'notify-detailed', ...
		'SpecifyObjectiveGradient', withgradient, ...
		'OptimalityTolerance', 1e-9, ...
		'PlotFcns',@optimplotfval);
	EVAL = iif(withgradient, @evalGradEsub, @evalEsub);
	
	ctr = 0; % iteration counter
	while true
		ctr = ctr + 1;

		disp(['Start of iteration ', num2str(ctr), '/', num2str(numiterations) , ' ...']);
		disp(['         E: ', sprintf(format, evalE(hatu, c))]);
		disp(['  norm L^2: ', sprintf(format, normL2(hatu, c))]);
		disp(['  norm H^1: ', sprintf(format, normH1(hatu, c))]);
		if withgradient
			grad = evalGradE(hatu, c);
			disp(['  grad L^2: ', sprintf(format, normGradL2(grad, c))]);
			disp(['  grad H^1: ', sprintf(format, normGradH1(grad, c))]);
		end
		disp(' ');
		

		r = coefftoreal(hatu);
		oldhatu = hatu;
		r = fminunc(@(r) EVAL(r, c), r, options);
		hatu = realtocoeff(r);
		diff = hatu - oldhatu;

		disp(['End of iteration ', num2str(ctr), '/', num2str(numiterations), ' ...']);
		disp(['         E: ', sprintf(format, evalE(hatu, c))]);
		disp(['  norm L^2: ', sprintf(format, normL2(hatu, c))]);
		disp(['  norm H^1: ', sprintf(format, normH1(hatu, c))]);
		disp(['  dist L^2: ', sprintf(format, normL2(diff, c))]);
		disp(['  dist H^1: ', sprintf(format, normH1(diff, c))]);
		if withgradient
			grad = evalGradE(hatu, c);
			disp(['  grad L^2: ', sprintf(format, normGradL2(grad, c))]);
			disp(['  grad H^1: ', sprintf(format, normGradH1(grad, c))]);
		end
		disp(' ');
		
		% generate constants for next step
		nextc = c;
		if (c.numcells < numcells) && (c.K == K || fct_preferaddcells(c))
			nextc.numcells = min(2 * c.numcells, numcells);
			disp('Increasing space dimension ...');
		elseif c.K < K
			nextc.K = min(2 * c.K, K);
			if c.evalwithfft
				timedim = fct_timedim(c);
				nextc.timedim_E = timedim;
				nextc.timedim_GradE = timedim;
			end
			disp('Increasing frequency dimension ...');
		else
			disp('Minimization complete');
			disp(' ');
			hatu = realtocoeff(r);
			return
		end
		nextc = bakeconstants(nextc);
		nexthatu = extrapolate(nextc, hatu, c);
		c = nextc;
		hatu = nexthatu;

		disp([' total dim: ' num2str(numel(r))]);
		disp(' ');
	end
end