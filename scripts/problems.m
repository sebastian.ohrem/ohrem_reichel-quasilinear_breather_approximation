function pbs = problems()
	models = {'radial', 'slab'};
	potentials = {'singlestep', 'periodicstep'};
	nonlinearities = {'timeaveraged', 'instantaneous'}; 
	symmetries = {'even'};

	params = {models, potentials, nonlinearities, symmetries};

	pbs = zeros(0, 1);
	for param = params
		param = param{:}; %#ok<FXSET>
		pbs = [repmat(pbs, size(param, 1, 2)); repelem(param, size(pbs, 2))];
	end
end