% REQUIRES
%	hatu, timeorder, x_plot, timedim, imagename, c

disp('Plotting breather profile ...');
disp(' ');
plotbreather3d(hatu, timeorder, x_plot, timedim, c);
disp('Saving breather image to disc ...');
disp(' ');
savefigure(gcf, imagename, true, [], []);