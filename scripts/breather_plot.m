% REQUIRES
%	xmax, timeorder, hatu, c

spacedim_plot = 3000;
x_plot = linspace(0, xmax, spacedim_plot)';
timedim_plot = 500;
plotbreather3d(hatu, timeorder, x_plot, timedim_plot, c);