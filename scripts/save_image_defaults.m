xmax = c.R + iif(strcmp(potential, 'periodicstep'), 10, 6);
x_plot = linspace(0, xmax, 3000)';
timedim = 512;
timeorder = 1;
imagename = [getwsym(timeorder), ' (', model, ',', potential, ',', nonlinearity, ',', symmetry, ')'];