% REQUIRES
%	hatu, c

xmax = c.R + 6;
x = linspace(0, xmax, 500)';
fps = 32;
periodtime = 16;
timeorder = 1;
playbreather(hatu, timeorder, x, fps, periodtime, c);