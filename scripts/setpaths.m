function setpaths(model, nonlinearity, potential, symmetry)
	% DESCRIPTION
	%	sets MATLAB path for provided setting
	global basicpath %#ok<GVMIS>
	persistent last_model last_nonlinearity last_potential last_symmetry

	firstrun = false;
	if isempty(basicpath)
		firstrun = true;
		% set persistent path on first execution of setpaths
		basicpath = [fileparts(mfilename('fullpath')), filesep, '..', filesep];
		addpath([basicpath, 'bessel']);
		addpath([basicpath, 'dG']);
		addpath([basicpath, 'dG', filesep, 'sources']);
		addpath([basicpath, 'problem']);
		addpath([basicpath, 'scripts'])
		addpath([basicpath, 'utility']);
		addpath([basicpath, 'visualization']);
	end

	if ~strcmp(potential, last_potential) || ~strcmp(model, last_model)
		if ~firstrun
			rmpath([basicpath, 'problem', filesep, 'potential', filesep, last_potential, filesep, last_model]);
		end

		addpath([basicpath, 'problem', filesep, 'potential', filesep, potential, filesep, model]);
	end

	if ~strcmp(model, last_model)
		if ~firstrun
			rmpath([basicpath, 'problem', filesep, 'model', filesep, last_model]);
		end
		
		addpath([basicpath, 'problem', filesep, 'model', filesep, model]);
		last_model = model;
	end

	if ~strcmp(nonlinearity, last_nonlinearity)
		if ~firstrun
			rmpath([basicpath, 'problem', filesep, 'nonlinearity', filesep, last_nonlinearity]);
		end
		
		addpath([basicpath, 'problem', filesep, 'nonlinearity', filesep, nonlinearity]);
		last_nonlinearity = nonlinearity;
	end

	if ~strcmp(potential, last_potential)
		if ~firstrun
			rmpath([basicpath, 'problem', filesep, 'potential', filesep, last_potential]);
		end
		
		addpath([basicpath, 'problem', filesep, 'potential', filesep, potential]);
		last_potential = potential;
	end

	if ~strcmp(symmetry, last_symmetry)
		if ~firstrun
			rmpath([basicpath, 'problem', filesep, 'symmetry', filesep, last_symmetry]);
		end
		
		addpath([basicpath, 'problem', filesep, 'symmetry', filesep, symmetry]);
		last_symmetry = symmetry;
	end
end