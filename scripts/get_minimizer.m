% REQUIRES
%	model
%	nonlinearity
%	potential
%	symmetry
%	dataname [optional]
%	fct_modifyc [optional]
%	K
%	numcells
%	fct_timedim [optional]
%	fct_preferaddcells [optional]
%	fct_startingpoint [optional]
%	withgradient [optional]
% PRIOVIDES
%	hatu
%	c

setpaths(model, nonlinearity, potential, symmetry);

if exist('fct_timedim', 'var') == 0
	fct_timedim = @(c) 16 * c.K;
end
if exist('fct_preferaddcells', 'var') == 0
	fct_preferaddcells = @(c) c.numcells < 4 * c.K;
end
if exist('fct_startingpoint', 'var') == 0
	fct_startingpoint = iif(strcmp(nonlinearity, 'timeaveraged'), @randomstart, @negativestart);
end
if exist('withgradient', 'var') == 0
	withgradient = true;
end

loaded = false;
if exist('dataname', 'var')
	[loaded, hatu, c] = tryloaddata(dataname);
end
if loaded
	% check that data is compatible
	if ~strcmp(model, c.model) ...
		|| ~strcmp(nonlinearity, c.nonlinearity) ...
		|| ~strcmp(potential, c.potential) ...
		|| ~strcmp(symmetry, c.symmetry) ...
		|| K ~= c.K ...
		|| numcells ~= c.numcells
		
		error('Data loaded inconsistent with parameters');
	else
		% warning('Data loaded may be inconsistent with parameters')
	end
else
	c = defaultconstants();
	if exist('fct_modifyc', 'var') ~= 0
		c = fct_modifyc(c);	
	end
	c = bakeconstants(c);
	
	hatu = fct_startingpoint(c);

	[hatu, c] = minimize(hatu, c, numcells, K, withgradient, fct_preferaddcells, fct_timedim);

	if exist('dataname', 'var') ~= 0
		savedata(dataname, hatu, c);
	end
end
clear loaded