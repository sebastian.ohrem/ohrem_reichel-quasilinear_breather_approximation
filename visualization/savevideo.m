function savevideo(hatu, timeorder, x, fps, periodtime, name, c, transparent, video)
	timedim = round(fps * periodtime);
	w = evalDw(hatu, timeorder, x, timedim, c);
	wmax = 1.1 * absmax(w) + eps;
	xmax = max(x);
	spf = iif(video, 1 / fps, []);

	for l = 1:timedim
		clf
		hold on
		ylim([-wmax, wmax]);
		xlim([0, xmax]);
		plot(x, w(:, l), 'k-');
		plotpotentials2d(xmax, wmax, c);
		ax = gca;
		ax.Color = 'none';

		savefigure(gcf, name, transparent, l, spf);
	end
end