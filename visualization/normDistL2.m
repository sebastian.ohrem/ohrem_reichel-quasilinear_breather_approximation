function y = normDistL2(hatu, c)
	% DESCRIPTION
	%	calculates the L2 norm squares of each frequency component of function u
	y = zeros(1, c.K);
	for k = 1:2:c.K
		l = (k + 1) / 2;
		y(k) = hatu(:, l)' * c.massl2 * hatu(:, l);
	end
	y = 2 * real(y);
end