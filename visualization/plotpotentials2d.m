function plotpotentials2d(xmax, wmax, c)
	orange = [0.8500, 0.3250, 0.0980];
	blue = [0, 0.4470, 0.7410];

	[xChi1, yChi1] = potentialChi1(xmax, c, false);
	[xChi3, yChi3] = potentialChi3(xmax, c, false);

	% rescale
	scale = 0.7 * wmax / absmax(yChi1, yChi3);
	yChi1 = scale * yChi1;
	yChi3 = scale * yChi3;

	plot(xChi1, yChi1, 'Color', orange, 'LineStyle', '-');
	plot(xChi3, yChi3, 'Color', blue, 'LineStyle', '-.');
end