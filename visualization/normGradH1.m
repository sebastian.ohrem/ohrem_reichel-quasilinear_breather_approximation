function y = normGradH1(grad, c)
	% INPUT
	%	grad (:,:) double [l2 gradient]
	% DESCRIPTION
	%	evalutes H1 dot norm of the H1 dot gradient

	y = 0;
	for k = 1:2:c.K
		l = (k + 1) / 2;
		y = y + grad(:, l)' * (((c.omega * k)^2 * c.massl2 + c.massh1) \ grad(:, l));
	end
	y = sqrt(real(y / 2));
end