function plotbreather3d(hatu, timeorder, x, timedim, c)
	% DESCRIPTION
	%	creates a surface plot of the breather

	orange = [0.8500, 0.3250, 0.0980];
	blue = [0, 0.4470, 0.7410];

	fontsize = iif(strcmp(c.potential, 'singlestep'), 23, 20);
	% fontsize = 16;

	% add negative values
	[w, t] = evalDw(hatu, timeorder, x, timedim, c);
	w = [w(end:-1:2, :); w];
	x = [-x(end:-1:2, :); x];

	% shift, add 2nd period and 1 point overlap
	[~, i] = min(max(abs(w), [], 1), [], 2);
	w = [w(:,i:end), w, w(:,1:i)];
	t = [t(i:end), t + c.T, t(1:i) + 2 * c.T];

	numspace = numel(x);
	numtime = numel(t);
	wmax = absmax(w);
	xmin = x(1);
	xmax = x(end);
	xmid = (xmin + xmax) / 2;
	tmin = t(1);
	tmax = t(end);
	tmid = (tmin + tmax) / 2;

	fig = clf;
	set(fig, 'NumberTitle', 'off', ...
		'Name', ['breather (', c.model, ',', c.potential, ',', c.nonlinearity, ')']);
	hold on

	% plot potentials
	[xChi1, yChi1] = potentialChi1(xmax, c, true);
	[xChi3, yChi3] = potentialChi3(xmax, c, true);

	% rescale
	scale = 0.7 * wmax / absmax(yChi1, yChi3);
	yChi1 = scale * yChi1;
	yChi3 = scale * yChi3;
	tChi1 = zeros(size(xChi1)) + tmin;
	tChi3 = zeros(size(xChi3)) + tmin;

	plot3(xChi1, tChi1, yChi1, 'Color', orange, 'LineStyle', '-', 'LineWidth', 1.5);
	plot3(xChi3, tChi3, yChi3, 'Color', blue, 'LineStyle', '-.', 'LineWidth', 1.5);

	% plot surface
	[X, T] = meshgrid(x, t);
	X = X'; 
	T = T';

	% color
	C = zeros(numspace, numtime, 3) + 0.7;
	s = surf(X, T, w, C);
	s.EdgeAlpha = 0;
	s.FaceAlpha = 1;

	light('Style', 'infinite', 'Position', [0, -5, 1], 'Color', [1, 1, 1]);
	s.AmbientStrength = 0.4;
	s.DiffuseStrength = 0.6;
	s.SpecularStrength = 0.1;
	s.SpecularExponent = 10;
	s.SpecularColorReflectance = 1;

	% add bounding lines
	plot3(X(1,:), T(1,:), w(1,:), 'k-');
	plot3(X(:,1), T(:,1), w(:,1), 'k-');
	plot3(X(end,:), T(end,:), w(end,:), 'k-');
	plot3(X(:,end), T(:,end), w(:,end), 'k-');

	ax = gca;
	
	zratio = 4 * wmax / (tmax - tmin);
	ax.DataAspectRatio = [1, 1, zratio];
	
	ax.XGrid = "off";
	ax.YGrid = "off";
	ax.ZGrid = "off";
	
	ax.TickLabelInterpreter = "latex";
	ax.Color = 'none';
	ax.XColor = [0, 0, 0];
	ax.YColor = [0, 0, 0];
	ax.ZColor = [0, 0, 0];
	ax.FontSize = fontsize;

	xlim([xmin, xmax]);
	text(xmid, tmin, -0.85 * wmax, ['$', getxsym(), '$'], 'Interpreter', 'latex', 'FontSize', fontsize);
	[xChi1, xtl] = getxticks(xmax, c, true);
	xticks(xChi1);
	xticklabels(xtl);
	xtickangle(79.6859);

	ylim([tmin, tmax]);
	text(xmax -0.05 * c.T, tmid, - 0.85 * wmax, '$t$', 'Interpreter', 'latex', 'FontSize', fontsize);
	yticks([tmin, tmin + c.T, tmin + 2 * c.T]);
	yticklabels({'$0$', '$T$', '$2 T$'});
	
	wsym = getwsym(timeorder);
	zlim([-wmax, wmax]);
	zlabel(['$ ', wsym, ' ( ', getxsym(), ' ,t)$'], 'Interpreter', 'latex', 'FontSize', fontsize + 4);
	zticks([-wmax, 0, wmax]);
	zticklabels({['$-\left\Vert ', wsym, ' \right\Vert_\infty$'], '$0$', ['$\left\Vert ', wsym, ' \right\Vert_\infty$']})

	view(20, 30);
end