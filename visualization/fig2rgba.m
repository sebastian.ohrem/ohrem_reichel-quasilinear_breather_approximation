function [rgb, a] = fig2rgba(fig, transparent)
	res = 300;

	if ~transparent
		exportgraphics(fig, 'tmp.png', 'Resolution', res, 'ContentType', 'image', 'BackgroundColor', [1, 1, 1]);
		rgb = imread('tmp.png');
		delete 'tmp.png';
		s = size(rgb);
		a = ones(s(1),s(2));
	else
		% idea from https://stackoverflow.com/questions/16134642/how-to-save-figure-with-transparent-background
		w = 1;
		exportgraphics(fig, 'tmp.png', 'Resolution', res, 'ContentType', 'image', 'BackgroundColor', [w, w, w]);
		rgb_w = imread('tmp.png');
		delete 'tmp.png';

		b = 0.1;
		exportgraphics(fig, 'tmp.png', 'Resolution', res, 'ContentType', 'image', 'BackgroundColor', [b, b, b]);
		rgb_b = imread('tmp.png');
		delete 'tmp.png';

		rgb_w = double(rgb_w) / 255;
		rgb_b = double(rgb_b) / 255;
	
		% alpha
		a = 1 - (rgb_w - rgb_b) / (w - b);
		a = mean(a, 3);
		a = max(0, min(1, a));
		m = a > eps;
	 	
		% color
		rgb = zeros(size(rgb_w));
		for i = 1:3
			vi = w * rgb_b(:, :, i) - b * rgb_w(:, :, i);
    		ci = rgb(:, :, i);
    		ci(m) = vi(m) ./ a(m) / (w - b);
    		rgb(:, :, i) = ci;
		end
		rgb = max(0, min(1, rgb));
		rgb = uint8(255 * rgb);
	end
end