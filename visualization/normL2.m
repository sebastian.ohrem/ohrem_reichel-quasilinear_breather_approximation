function y = normL2(hatu, c)
	% DESCRIPTION
	%	calculates the L2 norm of the function u
	y = sqrt(sum(normDistL2(hatu, c)));
end