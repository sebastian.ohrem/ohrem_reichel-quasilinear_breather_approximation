function savefigure(fig, name, transparent, index, spf)
	% saves the figure <fig> into file saves/<name>
	%	use both index and spf to generate .gif
	%	use index, and spf=[] to generate sequency of images
	%	use index=[] and spf=[] to generate single image

	[rgb, a] = fig2rgba(fig, transparent);
	if ~isempty(spf)
		% generate gif
		if transparent
			error('transparent content not supported in gif');
		end
		path = getsavespath(name, '.gif');
		[A, map] = rgb2ind(rgb, 16);
		if index == 1
			if exist(path, 'file')
				delete(path)
			end
			imwrite(A, map, path, 'LoopCount', Inf, 'DelayTime', spf);
		else
			imwrite(A, map, path, 'WriteMode', 'append', 'DelayTime', spf);
		end
	else
		% generate single image
		if ~isempty(index)
			path = getsavespath([name, '-', num2str(index)], '.png');
		else
			path = getsavespath(name, '.png');
		end
		if exist(path, 'file')
			delete(path)
		end
		imwrite(rgb, path, 'Alpha', a);
	end
end