function y = normDistH1(hatu, c)
	% DESCRIPTION
	%	calculates the H1 dot norm squares of each frequency component of function u
	y = zeros(1, c.K);
	for k = 1:2:c.K
		l = (k + 1) / 2;
		y(k) = (c.omega * k)^2 * (hatu(:, l)' * c.massl2 * hatu(:, l));
		y(k) = y(k) + hatu(:, l)' * c.massh1 * hatu(:, l);
	end
	y = 2 * real(y);
end