function playbreather(hatu, timeorder, x, fps, periodtime, c)
	timedim = fps * periodtime;
	w = evalDw(hatu, timeorder, x, timedim, c);
	wmax = 1.1 * absmax(w) + eps;
	wsym = getwsym(timeorder);
	xmax = max(x);

	while true
		for l = 1:timedim
			tic
			clf
			hold on
			title(['$', wsym, '$'], 'Interpreter', 'latex');
			ylim([-wmax, wmax]);
			plot(x, w(:, l), 'k-');
			plotpotentials2d(xmax, wmax, c);
			pause(1 / fps - toc);
		end
	end
end