function [w, t] = evalDw(hatu, timeorder, x, timedim, c)
	% INPUT
	%	hatu (:,:) couplex double [coefficients]
	%	timeorder (1,1) double [order of the time-derivative to be evaluated]
	%	x (:,1) double [spatial eval points]
	%	timedim (1,1) double [number of temporal eval points]
	%	c (1,1) struct
	% OUTPUT
	%	w (:,:) double [eval of timeorder'th time-derivative of w]
	%	t (1,:) double [temporal eval points]

	eval_hatw = zeros(size(x, 1), c.frequencydim);
	cs1 = x < c.R;
	cs2 = ~cs1;

	% finite element part of coefficients
	eval_hatw(cs1, :) = evaluateCG(hatu, x(cs1, :), c);
	% continuation using fundamental solutions
	eval_hatw(cs2, :) = ((c.evalbd * hatu) ./ evaluatePhi(c.R, c.k, c)) .* evaluatePhi(x(cs2, :), c.k, c);
	
	if timeorder ~= 0
		eval_hatw = (1i * c.omega * c.k).^timeorder .* eval_hatw;
	end
	
	
	w = fs(eval_hatw, timedim, c);

	if nargout > 1
		t = c.T / timedim * (0:(timedim - 1));
	end
end