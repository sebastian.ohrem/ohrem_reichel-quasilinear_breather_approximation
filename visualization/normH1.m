function y = normH1(hatu, c)
	% DESCRIPTION
	%	calculates the H1 dot norm of function u
	y = sqrt(sum(normDistH1(hatu, c)));
end