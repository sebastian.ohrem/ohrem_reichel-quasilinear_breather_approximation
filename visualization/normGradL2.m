function y = normGradL2(grad, c)
	% INPUT
	%	grad (:,:) double [l2 gradient]
	% DESCRIPTION
	%	evalutes the L2-norm of the L2-gradient

	y = 0;
	for k = 1:2:c.K
		l = (k + 1) / 2;
		y = y + grad(:,l)' * (c.massl2 \ grad(:,l));
	end
	y = sqrt(real(y / 2));
end