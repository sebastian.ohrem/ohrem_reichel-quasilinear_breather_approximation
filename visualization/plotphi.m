function plotphi(x, k, c, weighted, withpotentials)
	% DESCRIPTION
	%	plots the fundamental solutions phi_k
	
	assert(all(x >= c.R));

	phi = evaluatePhi(x, k, c);

	if weighted
		if strcmp(c.potential, 'periodicstep')
			decay = min(sqrt(c.alpha / c.beta), sqrt(c.beta / c.alpha));
			phi = phi .* exp(-log(decay) / c.P * x);
		end
		if strcmp(c.type, 'radial')
			phi = phi .* sqrt(x);
		end
	end

	% normalize to fit in common plot
	phi = phi ./ max(abs(phi), [], 1);

	clf
	plot(x, phi);

	if withpotentials
		hold on
		ymax = 1.1;
		xmin = min(x);
		xmax = max(x);
		plotpotentials(xmax, ymax, c);
		xlim([xmin, xmax]);
	end
end