function displayinfo(hatu, c)
	% DESCRIPTION
	%   displays misc info on the breather
	format = '%.8f';

	%% how is mass distributed (in frequency modes)?
	disp('Most weighted frequency is ... (with rest of order)')
	[n, k] = max(normDistL2(hatu, c));
	disp(['       L2: k=', num2str(k), ' (', sprintf(format, 1 - n / normL2(hatu, c)^2), ')']);
	[n, k] = max(normDistH1(hatu, c));
	disp(['       H1: k=', num2str(k), ' (', sprintf(format, 1 - n / normH1(hatu, c)^2), ')']);

	if strcmp(c.symmetry, 'none')
		%% how even is breather (up to symmetries)?
		if strcmp(c.nonlinearity, 'instantaneous')
			% symmetry is time shifts
			int = hatu(:, 1).' * c.massl2 * hatu(:, 1);
			angle = iif(int ~= 0, sqrt(sign(int)), 1);
			angles = angle .^ c.k;
		else
			% symmetry is shifts for each frequency
			angles = zeros(1, c.frequencydim);
			for l = 1:c.frequencydim
				int = hatu(:, l).' * c.massl2 * hatu(:, l);
				angles(l) = iif(int ~= 0, sqrt(sign(int)), 1);
			end
		end

		hatu_shift = hatu .* conj(angles);
		hatu_shift_odd = 1i * imag(hatu_shift);
	
		disp('defect from even breather (up to symmetry) is of size ...');
		disp(['      L2: ', sprintf(format, normL2(hatu_shift_odd, c) / normL2(hatu, c))]);
		disp(['      H1: ', sprintf(format, normH1(hatu_shift_odd, c) / normH1(hatu, c))]);
	end
end
