function c = bakeconstants(c)
	% DESCRIPTION
	%	performs checks on problem constants,
	%	generates problem parameters that can be calculated from other parameters

	assert(mod(c.K, 2) == 0);

	% problem parameters
	c.omega = 2 * pi / c.T; % base frequency
	c.frequencydim = c.K / 2;
	c.k = 1:2:c.K; % values of k
	c.d = c.c^(-2)-1-c.delta;

	% finite element related
	domain = dGdomain();
	domain.tag = 'I';
	domain.ncpd = c.numcells;
	domain.geom = [c.R / 2, c.R];
	c.mesh = dGdomain(domain); % mesh
	c.space = cGspace(c.mesh, c.polydegree); % finite element space
	c.spacedim = c.space.Ncnod;
	
	% x values for evaluation of cG space: evaluate(c, x(i)) = c(i)
	c.x = zeros(c.spacedim, 1); 
	c.x(c.space.Ec * (1:c.spacedim)') = c.space.x(:);
	
	% mass matrices
	c.massl2 = []; % l2 inner product
	c.massh1 = []; % h1 dot inner product
	c.mass0 = []; % quadrativ term of E with 0th order time derivative
	c.mass2 = []; % quadrativ term of E with 2nd order time derivative
	
	c.evalbd = sparse(1, c.spacedim); % eval at right boundary c.R
	c.evalbd(end) = 1;

	c.massbd = c.evalbd' * c.evalbd; % mass of eval at right boundary

	% setting-dependant constants are set by corresponding functions
	c = bakeconstants_model(c); % mass matrices (see above) are set here
	c = bakeconstants_nonlinearity(c);
	c = bakeconstants_potential(c);
	c = bakeconstants_symmetry(c);
	c = bakeconstants_outside(c);
end