function av = ut_square_av(hatu, c)
	% DESCRIPTION
	%	calculates the time-average of ut^2 over 1 period

	av = 2 * sum(((c.omega * c.k) .* abs(hatu)).^2 , 2);
end