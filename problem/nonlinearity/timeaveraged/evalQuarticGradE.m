function grad = evalQuarticGradE(hatu, c)
	av = ut_square_av(hatu, c);
	grad = (2 * c.gamma) * (c.omega * c.k).^2 .* (c.massl2 * (av .* hatu));
end