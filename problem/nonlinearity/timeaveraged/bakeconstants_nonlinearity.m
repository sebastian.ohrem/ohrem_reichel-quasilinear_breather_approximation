function c = bakeconstants_nonlinearity(c)
	c.nonlinearity = 'timeaveraged';
	c.evalwithfft = false; % whether to use an fft approx in the calculation of E and GradE
end