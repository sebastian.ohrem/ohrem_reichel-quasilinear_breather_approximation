function y = evalQuarticE(hatu, c)
	av = ut_square_av(hatu, c);
	y = av' * c.massl2 * av;
	y = c.gamma / 4 * y;
end