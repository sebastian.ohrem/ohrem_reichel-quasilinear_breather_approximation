function grad = evalQuarticGradE(hatu, c)
	hat_ut = 1i * c.omega * c.k .* hatu;
	ut = fs(hat_ut, c.timedim_GradE, c);
	ut_cube = ut.^3;
	hat_ut_cube = ifs(ut_cube, c);

	grad = (-2 * 1i * c.gamma * c.omega) * c.k .* (c.massl2 * hat_ut_cube);
end