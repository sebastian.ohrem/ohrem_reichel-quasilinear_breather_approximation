function y = evalQuarticE(hatu, c)
	ft = 1i * c.omega * c.k .* hatu;

	y = 0;
	% we evaluate ut and numerically intergrate ut^4
	ut = fs(ft, c.timedim_E, c);
	ut_square = ut.^2;
	for l = 1:c.timedim_E
		y = y + ut_square(:, l)' * c.massl2 * ut_square(:, l);
	end
	y = c.gamma / (4 * c.timedim_E) * y; 
end