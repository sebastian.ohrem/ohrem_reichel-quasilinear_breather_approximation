function c = bakeconstants_nonlinearity(c)
	c.nonlinearity =  'instantaneous';
	c.evalwithfft = true; % whether to use an fft approx in the calculation of E and GradE
	
	assert(c.timedim_E >= c.K);
	assert(c.timedim_GradE >= c.K);
end