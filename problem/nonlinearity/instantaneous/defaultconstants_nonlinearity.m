function c = defaultconstants_nonlinearity(c)
	c.timedim_E = 16 * c.K; % number of temporal eval points for calc of E
	c.timedim_GradE = 16 * c.K; % number of temporal eval points for calc of GradE
end