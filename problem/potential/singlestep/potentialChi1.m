function [X, Y] = potentialChi1(xmax, c, includenegative)
	[X, Y] = hline(0, c.R, c.d);
	[X, Y] = addhline(X, Y, c.R + c.rho, c.a);
	[X, Y] = addhline(X, Y, xmax, c.b);

	if includenegative
		X = [-X(end:-1:2), X];
		Y = [Y(end:-1:2), Y];
	end
end
