function c = bakeconstants_potential(c)
	c.potential = 'singlestep';

	c.alpha = (c.m * pi / (2 * c.n * c.omega * c.rho))^2;

	c.a = c.c^(-2) - 1 + c.alpha;
	c.b = c.c^(-2) - 1 - c.beta;
end