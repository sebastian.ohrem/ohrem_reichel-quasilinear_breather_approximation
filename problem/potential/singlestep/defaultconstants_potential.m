function c = defaultconstants_potential(c)
	c.delta = 1/10;

	c.beta = 1;
	c.m = 1; % integer
	c.n = 1; % integer
	c.rho = 1;
end