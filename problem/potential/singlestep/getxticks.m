function [points, labels] = getxticks(~, c, includenegative)
	points = [-c.R - c.rho, -c.R, 0, c.R, c.R + c.rho];
	labels = {'$-R-\rho$', '$-R$', '$0$', '$R$', '$R+\rho$'};

	if ~includenegative
		points = points((end+1)/2:end);
		labels = labels((end+1)/2:end);
	end
end 