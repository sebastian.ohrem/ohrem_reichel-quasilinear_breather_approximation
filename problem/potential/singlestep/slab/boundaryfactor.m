function y = boundaryfactor(k, c)
	y = c.omega * sqrt(c.alpha) * abs(k) .* cot(c.m * pi / (2 * c.n) * abs(k) + c.vartheta);
end