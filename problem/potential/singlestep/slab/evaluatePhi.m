function phi = evaluatePhi(x, k, c)
	cs1 = x < c.R + c.rho;
	cs2 = ~cs1;

	phi = zeros(size(x, 1), size(k, 2));

	alpha_k = c.omega * sqrt(c.alpha) * abs(k);
	beta_k = c.omega * sqrt(c.beta) * abs(k);

	phi(cs1, :) = sqrt(1 + c.beta / c.alpha) * ...
		sin(c.vartheta + (c.R + c.rho - x(cs1, :)) * alpha_k);
	phi(cs2, :) = exp(- (x(cs2, :) - c.R - c.rho) * beta_k);
end