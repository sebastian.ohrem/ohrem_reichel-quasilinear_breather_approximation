function c = bakeconstants_outside(c)
	c.vartheta = atan(sqrt(c.alpha / c.beta));
end