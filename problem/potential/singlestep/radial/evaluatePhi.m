function phi = evaluatePhi(x, k, c)
	k = abs(k);
	
	alpha_k = c.omega * sqrt(c.alpha) * k;
	beta_k = c.omega * sqrt(c.beta) * k;

	cs1 = x < c.R + c.rho;
	cs2 = ~cs1;

	phi = zeros(size(x, 1), size(k, 2));

	phi(cs1, :) = c.A(k) .* J1(x(cs1, :) * alpha_k) ...
		+ c.B(k) .* Y1(x(cs1, :) * alpha_k);
	phi(cs2, :) = K1(x(cs2, :) * beta_k);
end