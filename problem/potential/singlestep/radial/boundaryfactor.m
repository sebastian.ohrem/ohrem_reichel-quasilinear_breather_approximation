function y = boundaryfactor(k, c)
	k = abs(k);

	alpha_k = c.omega * sqrt(c.alpha) * k;
	z = c.R * alpha_k;

	y = - c.R * alpha_k .* (c.A(k) .* J1prime(z) + c.B(k) .* Y1prime(z)) ./ ...
		(c.A(k) .* J1(z) + c.B(k) .* Y1(z));
end