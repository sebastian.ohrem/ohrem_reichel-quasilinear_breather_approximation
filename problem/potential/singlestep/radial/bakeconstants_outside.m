function c = bakeconstants_outside(c)
	c.A = zeros(1, c.K - 1);
	c.B = zeros(1, c.K - 1);
	for k = 1:2:c.K
		alpha_k = k * c.omega * sqrt(c.alpha);
		beta_k = k * c.omega * sqrt(c.beta);
		za = alpha_k * (c.R + c.rho);
		zb = beta_k * (c.R + c.rho);

		AB = [J1(za), Y1(za); alpha_k * J1prime(za), alpha_k * Y1prime(za)] \ [K1(zb); beta_k * K1prime(zb)];
		c.A(k) = AB(1);
		c.B(k) = AB(2);
	end
end