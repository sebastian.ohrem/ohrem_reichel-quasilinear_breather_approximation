function c = bakeconstants_outside(c)
	c.A = zeros(c.numperiods, c.K - 1, 2);
	c.B = zeros(c.numperiods, c.K - 1, 2);
	sigma = (-1)^((c.m_alpha + c.m_beta) / 2);

	for k = 1:2:c.K
		alpha_k = k * c.omega * sqrt(c.alpha);
		beta_k = k * c.omega * sqrt(c.beta);
		
		x = c.R + (c.numperiods + c.theta / 2) * c.P;
		if c.alpha > c.beta
			F = 1 / sqrt(x) * [1; 0];
			decay = sigma * sqrt(c.beta / c.alpha);
		else
			F = 1 / sqrt(x) * [0; 1];
			decay = sigma * sqrt(c.alpha / c.beta);
		end
		AB = fundamental(alpha_k, x) \ F;

		for j = c.numperiods:-1:1
			x = c.R + (j - c.theta / 2) * c.P;
			F = fundamental(alpha_k, x) * AB;
			AB = fundamental(beta_k, x) \ F;
			c.A(j, k, 2) = decay^j * AB(1);
			c.B(j, k, 2) = decay^j * AB(2);

			x = c.R + (j - 1 + c.theta / 2) * c.P;
			F = fundamental(beta_k, x) * AB;
			AB = fundamental(alpha_k, x) \ F;
			c.A(j, k, 1) = decay^j * AB(1);
			c.B(j, k, 1) = decay^j * AB(2);

			AB = decay * AB;
		end
	end
end

function Y = fundamental(c_k, r)
	z = c_k * r;
	Y = [J1(z), Y1(z); c_k * J1prime(z), c_k * Y1prime(z)];
end