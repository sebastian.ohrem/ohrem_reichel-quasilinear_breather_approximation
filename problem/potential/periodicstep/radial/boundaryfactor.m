function y = boundaryfactor(k, c)
	k = abs(k);

	alpha_k = c.omega * sqrt(c.alpha) * k;
	z = c.R * alpha_k;

	y = - c.R * alpha_k .* (c.A(1, k, 1) .* J1prime(z) + c.B(1, k, 1) .* Y1prime(z)) ./ ...
		(c.A(1, k, 1) .* J1(z) + c.B(1, k, 1) .* Y1(z));
end