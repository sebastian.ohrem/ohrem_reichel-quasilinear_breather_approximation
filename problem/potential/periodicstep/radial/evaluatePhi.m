function phi = evaluatePhi(x, k, c)
	phi = zeros(size(x, 1), size(k, 2));

	n = floor((x - c.R) / c.P + c.theta / 2);
	xmid = c.R + (n + c.theta / 2) * c.P;
	xdiff = x - xmid;
	cs1 = (xdiff < 0) & (n < c.numperiods) & (n >= 0);
	cs2 = (xdiff >= 0) & (n < c.numperiods) & (n >= 0);
	cs3 = (n >= c.numperiods) | (n < 0);

	alpha_k = c.omega * sqrt(c.alpha) * k;
	beta_k = c.omega * sqrt(c.beta) * k;
	j = n + 1;

	phi(cs1, :) = c.A(j(cs1, :), k, 1) .* J1(x(cs1, :) * alpha_k) ...
		+ c.B(j(cs1, :), k, 1) .* Y1(x(cs1, :) * alpha_k);
	phi(cs2, :) = c.A(j(cs2, :), k, 2) .* J1(x(cs2, :) * beta_k) ...
		+ c.B(j(cs2, :), k, 2) .* Y1(x(cs2, :) * beta_k);
	phi(cs3, :) = NaN;
end