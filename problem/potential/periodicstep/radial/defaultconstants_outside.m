function c = defaultconstants_outside(c)
	c.numperiods = 100; % number of (spatial) periods used to approximate eigenfunction
end