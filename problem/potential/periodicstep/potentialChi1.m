function [X, Y] = potentialChi1(xmax, c, includenegative)
	[X, Y] = hline(0, c.R, c.d);
	n = 0;
	while true
		x = c.R + (n + c.theta / 2) * c.P;
		if x >= xmax
			[X, Y] = addhline(X, Y, xmax, c.a);
			break
		else
			[X, Y] = addhline(X, Y, x, c.a);
		end

		x = c.R + (n + 1 - c.theta / 2) * c.P;
		if x >= xmax
			[X, Y] = addhline(X, Y, xmax, c.b);
			break
		else
			[X, Y] = addhline(X, Y, x, c.b);
		end
		n = n + 1;
	end

	if includenegative
		X = [-X(end:-1:2), X];
		Y = [Y(end:-1:2), Y];
	end
end
