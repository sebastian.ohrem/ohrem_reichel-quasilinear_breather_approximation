function phi = evaluatePhi(x, k, c)
	phi = zeros(size(x, 1), size(k, 2));

	alpha_k = c.omega * sqrt(c.alpha) * k;
	beta_k = c.omega * sqrt(c.beta) * k;

	if c.alpha > c.beta
		n = floor((x - c.R) / c.P + c.theta / 2);
		xmid = c.R + (n + c.theta / 2) * c.P;
		xdiff = x - xmid;
		cs1 = (xdiff < 0);
		cs2 = ~cs1;

		phi(cs1, :) = cos(xdiff(cs1, :) * alpha_k);
		phi(cs2, :) = cos(xdiff(cs2, :) * beta_k);

		decay = c.sigma * sqrt(c.beta / c.alpha);
		phi = decay.^n .* phi;
	else
		n = floor((x - c.R) / c.P - c.theta / 2) + 1;
		xmid = c.R + (n - c.theta / 2) * c.P;
		xdiff = x - xmid;
		cs1 = xdiff < 0;
		cs2 = ~cs1;
	
		phi(cs1, :) = cos(xdiff(cs1, :) * beta_k);
		phi(cs2, :) = cos(xdiff(cs2, :) * alpha_k);

		decay = c.sigma * sqrt(c.alpha / c.beta);
		phi = decay.^n .* phi;
	end
end