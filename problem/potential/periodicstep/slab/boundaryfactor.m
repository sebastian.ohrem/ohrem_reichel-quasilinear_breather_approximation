function y = boundaryfactor(k, c)
	y = c.boundaryweight * k .* (-1).^((k + c.m_alpha) / 2);
end