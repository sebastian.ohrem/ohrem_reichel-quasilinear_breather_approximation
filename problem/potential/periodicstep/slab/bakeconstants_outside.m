function c = bakeconstants_outside(c)
	c.boundaryweight = iif(c.alpha > c.beta, 1, -1) * c.omega * sqrt(c.alpha);

	c.sigma = (-1)^((c.m_alpha + c.m_beta) / 2);
end