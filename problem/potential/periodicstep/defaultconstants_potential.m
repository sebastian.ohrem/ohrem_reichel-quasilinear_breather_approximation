function c = defaultconstants_potential(c)
	c.theta = 2/5; 
	c.P = 2;
	c.m_alpha = 1; % odd integer
	c.m_beta = 1; % odd integer
end