function c = bakeconstants_potential(c)
	c.potential = 'periodicstep';
	
	assert(mod(c.m_alpha, 2) == 1);
	assert(mod(c.m_beta, 2) == 1);
	
	c.alpha = (c.m_alpha * pi / (2 * c.P * c.theta * c.omega))^2;
	c.beta = (c.m_beta * pi / (2 * c.P * (1 - c.theta) * c.omega))^2;

	assert(c.alpha ~= c.beta);
	
	c.a = c.c^(-2) - 1 + c.alpha;
	c.b = c.c^(-2) - 1 + c.beta;
end