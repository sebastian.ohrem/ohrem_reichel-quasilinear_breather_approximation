function [points, labels] = getxticks(xmax, c, includenegative)
	points = [-c.R, 0, c.R];
	labels = {'$-R$', '$0$', '$R$'};
	
	n = 1;
	while true
		x = c.R + n * c.P;
		if x > xmax
			break
		else
			% modified
			% strn = [];
			% if n ~= 1
			% 	strn = num2str(n);
			% end
			strn = iif(n==1,'',num2str(n));
			
			%#ok<*AGROW>
			points = [-x, points, x]; 
			labels = [['$-R-',strn,'P$'], labels, ['$R+',strn,'P$']];
		end
		n = n + 1;
	end

	if ~includenegative
		points = points((end+1)/2:end);
		labels = labels((end+1)/2:end);
	end
end