function y = evalE(hatu, c)
	% DESCRIPTION
	%	evaluates E(u)
	
	y = evalQuadraticE(hatu, c) + evalQuarticE(hatu, c);
end