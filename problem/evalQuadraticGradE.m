function grad = evalQuadraticGradE(hatu, c)
	% DESCRIPTION
	%	evaluates the l2 gradient of the quadratic part of E(u)

	grad = c.mass0 * hatu ...
		 + (c.omega * c.k).^2 .* (c.mass2 * hatu) ...
		 + boundaryfactor(c.k, c) .* (c.massbd * hatu);
	grad = 2 * grad;
end