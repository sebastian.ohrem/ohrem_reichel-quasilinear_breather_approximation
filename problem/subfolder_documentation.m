% functions found in subfolders are described here


%% subfolder model\<model>
% function y = ansatzfunction(x, k, c)
% DESCRIPTION
%	evaluates the ansatz function: scaled I_1 or cosh

% function c = bakeconstants_model(c)
% DESCRIPTION 
%	see bakeconstants

% function function s = getxsym()
% DESCRIPTION
%	returns the symbol of the x variable: 'x' or 'r'



%% subfolder nonlinearity\<nonlinearity>
% function c = bakeconstants_nonlinearity(c)
% DESCRIPTION 
%	see bakeconstants

% function c = defaultconstants_nonlinearity(c)
% DESCRIPTION 
%	see defaultconstants

% function y = evalQuarticE(hatu, c)
% DESCRIPTION
%	evaluates the quartic part of E(u)

% function grad = evalQuarticGradE(hatu, c)
% DESCRIPTION
%	evaluates the l2 gradient of the quartic part of E(u)



%% subfolder potential\<potential>
% function c = bakeconstants_potential(c)
% DESCRIPTION 
%	see bakeconstants

% function c = defaultconstants_potential(c)
% DESCRIPTION 
%	see defaultconstants

% function [points, labels] = getxticks(xmax, c, includenegative)
% DESCRIPTION
%	returns points and labels for x ticks
%	only positive x values are used unless includenegative=true

% function [X, Y] = potentialChi1(xmax, c, includenegative)
% DESCRIPTION
%	returns (x, y) pairs used to plot the linear potential \chi_1
%	only positive x values are used unless includenegative=true
	


%% subfolder potential\<potential>\<model>
% function c = bakeconstants_outside(c)
% DESCRIPTION 
%	see bakeconstants

% function c = defaultconstants_outside(c)
% DESCRIPTION 
%	see defaultconstants

% function y = boundaryfactor(k, c)
% DESCRIPTION
%	evaluates the multiplicative factor
%	- phi_k'(R) / phi_k(R) for slab,
%	- R * phi_k'(R) / phi_k(R) for radial
%	appearing in the boundary term

% function phi = evaluatePhi(x, k, c)
% DESCRIPTION
%	evaluates phi_k(x) on the grid given by x and k values



%% subfolder symmetry\<symmetry>
% function c = bakeconstants_symmetry(c)
% DESCRIPTION 
%	see bakeconstants

% function r = coefftoreal(hatu)
% function hatu = realtocoeff(r)
% DESCRIPTION
%	conversion between coefficients and real representation matrix in symmetry class