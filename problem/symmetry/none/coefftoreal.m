function r = coefftoreal(hatu)
	r = real(hatu);
	r(:,:,2) = imag(hatu);
end