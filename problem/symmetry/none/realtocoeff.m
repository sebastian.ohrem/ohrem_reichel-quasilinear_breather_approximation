function hatu = realtocoeff(r)
	hatu = r(:,:,1) + 1i * r(:,:,2);
end