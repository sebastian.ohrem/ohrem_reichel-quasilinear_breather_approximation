function gradr = evalGradE(hatu, c)
	% DESCRIPTION
	%	evaluates the l2 gradient of E
	gradr = evalQuadraticGradE(hatu, c) + evalQuarticGradE(hatu, c);
end