function hatu = negativestart(c)
	% DESCRIPTION
	%	generates a starting point where E attains negative values
	%	throws an error if E does not attain negative values

	for k = 1:2:c.K
		l = (k + 1) / 2;
		hatu = zeros(c.spacedim, c.frequencydim);
		hatu(:,l) = ansatzfunction(c.x, k, c);
		
		quad = evalQuadraticE(hatu, c);
		if quad >= 0
			continue
		else
			disp('functional E attains negative values at frequency');
			disp(['         k: ', num2str(k)]);
			disp(' ');
			
			quart = evalQuarticE(hatu, c);
			hatu = sqrt(- quad / (2 * quart)) * hatu;
			return
		end
	end
	error('functional E appears to be nonnegative');
end