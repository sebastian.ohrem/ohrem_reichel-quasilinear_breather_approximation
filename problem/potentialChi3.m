function [X, Y] = potentialChi3(xmax, c, includenegative)
	% DESCRIPTION
	%	returns (x, y) pairs used to plot the nonlinear potential Gamma
	
	[X, Y] = hline(0, c.R, -c.gamma);
	[X, Y] = addhline(X, Y, xmax, 0);

	if includenegative
		X = [-X(end:-1:2), X];
		Y = [Y(end:-1:2), Y];
	end
end

