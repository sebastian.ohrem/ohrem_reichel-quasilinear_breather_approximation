function hatu = randomstart(c)
	hatu = randn(c.spacedim, c.frequencydim);

	if strcmp(c.symmetry, 'none')
		hatu = hatu + 1i * randn(c.spacedim, c.frequencydim);
	end
	hatu = hatu / normH1(hatu, c);
end