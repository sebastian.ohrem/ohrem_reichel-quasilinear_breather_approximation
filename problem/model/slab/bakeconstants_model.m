function c = bakeconstants_model(c)
	c.model = 'slab';

	pde = dGpde(1, [], [], [], []);
	c.massh1 = cGassempde(c.mesh, c.space, pde);
	pde = dGpde([], [], 1, [], []);
	c.massl2 = cGassempde(c.mesh, c.space, pde);

	c.mass0 = c.massh1;
	c.mass2 = c.delta * c.massl2;
end