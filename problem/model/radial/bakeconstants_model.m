function c = bakeconstants_model(c)
	c.model = 'radial';
	
	pde = dGpde(@(x) x, [], [], [], []);
	c.massh1 = cGassempde(c.mesh, c.space, pde);
	pde = dGpde([], [], @(x) x, [], []);
	c.massl2 = cGassempde(c.mesh, c.space, pde);
	pde = dGpde([], [], @(x) 1 ./ x, [], []);
	othermass = cGassempde(c.mesh, c.space, pde);

	c.mass0 = c.massh1 + othermass;
	c.mass2 = c.delta * c.massl2;
end