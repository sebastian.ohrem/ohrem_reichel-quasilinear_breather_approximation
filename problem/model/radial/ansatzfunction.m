function y = ansatzfunction(x, k, c)
	y = I1(max(c.omega * c.delta * abs(k), 0.001) * x);
end