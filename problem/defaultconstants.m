function c = defaultconstants()
	% DESCRIPTION
	%	returns a struct with default initialized problem constants
	%	after modifying these values, call bakeconstants(c)

	c = struct();
	%% parameters of mathematical problem
	c.gamma = 1;
	c.delta = 1/2;
	c.T = 4;
	c.R = 2;
	c.c = 2/3;
	
	%% parameters of finite element space
	c.K = 2; % maximum frequency (even)
	c.numcells = 1; % number of (spatial) cells
	c.polydegree = 1; % polynomial degree for (spatial) cells

	% setting dependant constants are set by corresponding functions
	c = defaultconstants_nonlinearity(c);
	c = defaultconstants_potential(c);
	c = defaultconstants_outside(c);
end