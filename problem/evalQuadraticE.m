function y = evalQuadraticE(hatu, c)
	% DESCRIPTION
	%	evaluates the quadratic part of E(u)

	y = 0;
	for k = 1:2:c.K
		l = (k + 1) / 2;
		y = y + hatu(:, l)' * c.mass0 * hatu(:, l);
		y = y + (c.omega * k)^2 * (hatu(:, l)' * c.mass2 * hatu(:, l));
		y = y + boundaryfactor(k, c) * (hatu(:, l)' * c.massbd * hatu(:, l));
	end
	y = real(y); % imaginary part may be nonzero due to rounding errors
end