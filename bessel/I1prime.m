function y = I1prime(z)
	% derivative of modified bessel function of first kind I_1
	y = (besseli(0, z) + besseli(2, z)) / 2;
end