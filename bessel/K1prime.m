function y = K1prime(z)
	% derivative of modified bessel function of second kind K_1
	y = - (besselk(0, z) + besselk(2, z)) / 2;
end