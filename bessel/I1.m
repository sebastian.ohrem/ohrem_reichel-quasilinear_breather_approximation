function y = I1(z)
	% modified bessel function of first kind I_1
	y = besseli(1, z);
end