function y = K1(z)
	% modified bessel function of second kind K_1
	y = besselk(1, z);
end