function r = iif(condition, rtrue, rfalse)
	% inline if
	if condition
		r = rtrue;
	else
		r = rfalse;
	end
end
