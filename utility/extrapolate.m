function f = extrapolate(c, other_f, other_c)
	% DESCRIPTION
	%	extrapolates coefficients f with respect to constants c
	%	from coeff other_f with respect to other_c
	freqdim = min(c.frequencydim, other_c.frequencydim);

	f = zeros(c.spacedim, c.frequencydim);
	f(:, 1:freqdim) = evaluateCG(other_f(:,1:freqdim), c.x, other_c);
end