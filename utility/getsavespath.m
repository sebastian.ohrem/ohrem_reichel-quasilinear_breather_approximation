function path = getsavespath(name, ext)
	global basicpath %#ok<GVMIS>
	path = [basicpath, 'saves', filesep, name, ext];
end