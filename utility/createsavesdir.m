function createsavesdir()
	global basicpath %#ok<GVMIS>
	dir = [basicpath, 'saves', filesep];
	if ~exist(dir, "dir")
		mkdir(dir);
	end
end