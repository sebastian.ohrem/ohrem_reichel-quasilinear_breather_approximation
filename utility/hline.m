function [X, Y] = hline(l, r, y)
	% DESCRIPTON
	%	gives X Y coordinates of horizontal line, used for potential plotting
	X = [l, r];
	Y = [y, y];
end