function y = absmax(varargin)
	% INPUT
	%	double arrays (any number, any size)
	% DESCRIPTION
	%	returns maximum value, or -Inf
	y = -Inf;
	for j = 1:nargin
		arr = varargin{j};
		if ~isempty(arr)
			m = max(abs(arr), [], "all");
			y = max(y, m);
		end
	end
end