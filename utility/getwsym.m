function s = getwsym(timeorder)
	% DESCRIPTION
	%	returns latex symbol  d_t^order(w)
	allowW = false; % modify this here

	if timeorder >= 0
		if allowW && timeorder ~= 0
			base = 'W';
			timeorder = timeorder - 1;
		else
			base = 'w';
		end

		switch timeorder
			case 0
				s = base;
			case 1
				s = [base, '_t'];
			case 2
				s = [base, '_{tt}'];
			case 3
				s = [base, '_{ttt}'];
			otherwise
				s = ['\partial_t^{', num2str(timeorder), '}', base];
		end
	else
		s = ['\partial_t^{', num2str(timeorder), '}w'];
	end
end