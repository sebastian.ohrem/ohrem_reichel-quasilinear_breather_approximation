function [X, Y]= addhline(X, Y, r, y)
	% DESCRIPTON
	%	adds X Y coordinates of horizontal line, used for potential plotting
	X = [X, X(end), r];
	Y = [Y, y, y];
end