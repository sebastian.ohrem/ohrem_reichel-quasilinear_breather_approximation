function hatf = ifs(f, c)
	% INPUT
	%	f (:,:) double [function evaluation]
	%	c (1,1) struct
	% OUTPUT
	%	hatf (:,:) double [fouerier coefficients, dense representation]
	% DESCRIPTION
	%	reconstructs the fourier coefficient from evaluation at equidistant points

	timedim = size(f, 2);
	f_full = 1 / timedim * fft(f, [], 2);
	hatf = f_full(:,2:2:c.K);
end