function [b, hatu, c] = tryloaddata(name)
	path = getsavespath(name, '.mat');
	b = isfile(path);
	if b
		load(path, 'hatu', 'c');
	else
		hatu = [];
		c = [];
	end
end