function y = evaluateCG(f, x, c)
	% INPUT
	%	f (:,?) double [each column is coefficients of cG basis described in c.space)
	%	x (:,1) double
	%	c (1,1) struct
	% OUTPUT
	%	y (:,?) double [each column is evaluation of w at points x]
	% WARNING
	%	implemented only for uniform cells

	nodespercell = c.space.Nnpc;
	cellwidth = c.R / c.numcells;
	
	s = size(f);
	numspace = size(x,1);
	y = zeros([numspace, s(2:end)]);

	% discontinuous galerkin coefficients
	f_dG = zeros([c.space.Nnode, s(2:end)]);
	f_dG(:, :) = c.space.Ec * f(:, :);

	for j = 1:numspace
		if x(j) >= c.R
			index = c.numcells - 1;
			qq = 1;
		else
			index = floor(x(j) / cellwidth);
			qq = x(j) / cellwidth - index;
		end
		M = dGeval(c.mesh, c.space, [], qq);
		y(j, :) = M * f_dG((nodespercell*index+1):(nodespercell*(index+1)), :);
	end
end