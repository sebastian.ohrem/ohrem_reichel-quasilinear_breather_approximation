function f = fs(hatf, timedim, c)
	% INPUT
	%	hatf (:,:) double [fouier coeffients, dense representation]
	%	timedim (1,1) double [number of evaluation points]
	%	c (1,1) struct
	% OUTPUT
	%	f (:,:) double
	% DESCRIPTION
	%	evaluates the fourier series at timedim points

	f_full = zeros([1, 2] .* size(hatf));
	f_full(:, 2:2:c.K) = hatf;
	f = 2 * timedim * real(ifft(f_full, timedim, 2));
end